# avrdude

## Contenu du dossier

 - `avrdude` : fichier exécutable (pour GNU-Linux) d'avrdude étant modifié. Si le système est compatible, il est possible de directement copier celle ci, voir *Installation*
 - `avrdude.conf` : fichier de configuration pour avrdude, même si sa modification n'est pas nécessaire.
 - `stk500.patch` & `stk500v2.patch` : patchs à appliquer sur le code source d'`avrdude`, voir *Modification & recompilation de avrdude*

## Modification & recompilation de `avrdude`

Tout d'abord, il faut télécharger la dernière version de `avrdude`. Elle est disponible sur [la page nongnu du projet](http://savannah.nongnu.org/projects/avrdude).
Décompresser le `.tar.gz`, et puis appliquer les patchs `stk500.patch` et `stk500v2.patch` à l'aide des commandes suivantes :

> patch avrdude-X.X/stk500_private.h < stk500.patch
> patch avrdude-X.X/stk500v2_private.h < stk500v2.patch

Ensuite, il faut compiler `avrdude`, en éxécutant ces commandes dans le dossier où le code a été décompressé :

> ./configure
> make

_Note :_ Il sera peut-être néccessaire d'installer des dépendances, si `./configure` échoue.

Une fois que c'est effectué, `avrdude` est compilé !

## Installation

Pour pouvoir utiliser le bootloader sécurisé, il est nécessaire de remplacer la version présente dans arduino par `avrdude` fraîchement compilé (ou celui pré-compilé)
Il se situe dans (relatif au dossier racine d'arduino) : `hardware/tools/avr/bin/avrdude`.
Si le téléversement échoue à cause d'une erreur de syntaxe dans `avrdude.conf`, il faut remplacer `hardware/tools/avr/etc/avrdude.conf` par le fichier local.
