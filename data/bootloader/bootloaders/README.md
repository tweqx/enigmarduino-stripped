# Bootloader sécurisé

## Installation

Pour pourvoir l'écrire sur une arduino, il faut tout d'abord remplacer les fichiers correspondant au bootloader compilé dans les fichiers d'arduino.
À partir du dossier racine de l'installation d'arduino, remplacer :
  - Pour l'arduino UNO :
    `hardware/arduino/avr/bootloaders/optiboot/optiboot_atmega328_secured.hex` par le fichier local `UNO/optiboot_atmega328_secured.hex`
  - Pour l'arduino MEGA :
    `hardware/arduino/avr/bootloaders/stk500v2/stk500boot_v2_mega2560.hex` par le fichie local `MEGA/stk500boot_v2_mega2560.hex`

Voilà :)

## Écriture sur une arduino

Tout d'abord, il faut deux arduino : l'arduino dont le bootloader sera changé, l'arduino cible, et l'arduino qui aider à changer le bootloader de la cible, l'arduino programmateur.
Sur l'arduino programmateur, avec un IDE non modifié, il faut uploader le sketch `ArduinoISP` situé dans : `Fichier > Exemples > 11.ArduinoISP`.
Connectez les deux arduino ensembles, en respectant le schéma convenant à la situation (les condensateurs sont optionnels) :

!["Programmateur : UNO, Cible : UNO"](./UNOtoUNO.jpg "Programmateur : UNO, Cible : UNO")
!["Programmateur : MEGA, Cible : UNO"](./MegaToUNO.jpg "Programmateur : MEGA, Cible : UNO")
!["Programmateur : UNO, Cible : MEGA"](./UNOToMega.png "Programmateur : UNO, Cible : MEGA")

Sélectionnez dans le menu "Options" le programmateur "Arduino as ISP", et la version de l'arduino qui va recevoir le bootloader toujours dans le menu "Options".
Ensuite, sélectionnez "Graver la séquences d'initialisation", et laissez la magie opérer :)

## Compilation

T'as pas eu le courage d'écrire les READMEs jusqu'au bout, donc tu t'en souviendras comment compiler aussi... flemmard
