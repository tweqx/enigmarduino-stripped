import sys

if len(sys.argv) != 2:
	print("Usage: {progName} <chaîne de caractères à compresser>".format(progName=sys.argv[0]))
	print("")
	print("La chaîne de caractères à compresser doit seulement contenir les caractères suivants: ")
	print("' + - . | <retour à la ligne> <espace>")
	print("Elle peut aussi contenir entre 0 et 8 symboles AEBFCGDH, respectivement dans cette ordre")
	print("Ou bien:")
	print("  [1]")
	print("  [NON]")
	print("  [====]")
	print("  [=====]")
	print("  | ET |")
	print("  | OU |")
	print("  | XOR |")
	print("Ou la compression va échouer.")
	exit(1)

try:
	uncompressed_str = open(sys.argv[1], "r").read()
except IOError:
	print("Fichier introuvable/non lisible")
	exit(1)
i = 0

def readInc():
	global i
	global uncompressed_str

	if i >= len(uncompressed_str):
		print("Erreur de compression : fin de la chaîne de caractère inattendue")
		exit(1)
	c = uncompressed_str[i]
	i += 1
	return c

def read(offset):
	global i
	global uncompressed_str

	if i + offset >= len(uncompressed_str):
		print("Erreur de compression : Fin de chaîne inatendue")
		exit(1)
	return uncompressed_str[i + offset]

CHR_END          = '0000'
CHR_NEW_LINE     = '0001'
CHR_SPACE        = '0010'
CHR_SINGLE_QUOTE = '0011'
CHR_PLUS         = '0100'
CHR_DASH         = '0101'
CHR_POINT        = '0110'
CHR_VERTICAL     = '0111'
CHR_LETTER       = '1000'
CHR_CONST_1      = '1001'
CHR_NOT          = '1010'
CHR_AND          = '1011'
CHR_SMALL_EQUALS = '1100'
CHR_OR           = '1101'
CHR_XOR          = '1110'
CHR_BIG_EQUALS   = '1111'
out = ""

letters = ["A", "E", "B", "F", "C", "G", "D", "H"]
current_letter = 0
def isValidLetter(c):
	global current_letter
	global letters

	if c in letters:
		if current_letter >= len(letters):
			print("Lettre invalide : les lettres AEBFCGDH ont déjà été utilisées")
		elif letters[current_letter] == c:
			current_letter += 1
			return True
		else:
			print("Lettre invalide : la lettre n'est pas utilisé dans le bon ordre")
		exit(1)
	return False

def printer():
	global out

	progOutput = ""

	offset = 0
	while True:
		current_digit = ""
		for i in range(min(len(out[offset:]), 8)):
			current_digit += out[offset + i]

		if len(current_digit) < 8:
			current_digit += ''.join(["0" for _ in range(8 - len(current_digit))])
			progOutput += chr(int(current_digit, 2))
			print(progOutput)
			exit(0)
		else:
			progOutput += chr(int(current_digit, 2))
			offset += 8

while True:
	if i >= len(uncompressed_str):
		out += CHR_END
		printer()
		exit(0)
	c = readInc()
	#print(hex(ord(c)), "'" + c + "'")
	if c == "'":
		out += CHR_SINGLE_QUOTE
	elif c == "\n":
		out += CHR_NEW_LINE
	elif c == " ":
		out += CHR_SPACE
	elif c == "+":
		out += CHR_PLUS
	elif c == "-":
		out += CHR_DASH
	elif c == ".":
		out += CHR_POINT
	elif c == "|":
		if read(0) == " ":
                        if read(1) == "E" and read(2) == "T" and read(3) == " " and read(4) == "|":
                                i += 5
                                out += CHR_AND
                        elif read(1) == "O" and read(2) == "U" and read(3) == " " and read(4) == "|":
                                i += 5
                                out += CHR_OR
                        elif read(1) == "X" and read(2) == "O" and read(3) == "R" and read(4) == " " and read(5) == "|":
                                i += 6
                                out += CHR_XOR
                        else:
                                i += 1
                                out += CHR_VERTICAL
                                out += CHR_SPACE
		else:
			out += CHR_VERTICAL
	elif isValidLetter(c):
		out += CHR_LETTER
	elif c == "[":
		c = readInc()
		if c == "1" and read(0) == "]":
			i += 1
			out += CHR_CONST_1
		elif c == "=" and read(0) == "=" and read(1) == "=" and read(2) == "=":
			i += 3
			c = readInc()
			if c == "]":
				out += CHR_SMALL_EQUALS
			elif c == "=" and read(0) == "]":
				i += 1
				out += CHR_BIG_EQUALS
			else:
				print("Erreur de syntaxe # 4")
				exit(1)
		elif c == "N" and read(0) == "O" and read(1) == "N" and read(2) == "]":
			i += 3
			out += CHR_NOT
		else:
			print("Erreur de syntaxe # 2")
			exit(1)
	else:
		print("Erreur de syntaxe : Caractère '{}' ({}) non encodable".format(c, hex(ord(c))))
		exit(1)
