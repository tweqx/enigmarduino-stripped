import sys

if len(sys.argv) != 2:
	print("Usage: {progName} <chaîne de caractères à compresser>".format(progName=sys.argv[0]))
	exit(1)

try:
	uncompressed_str = open(sys.argv[1], "r").read()
except IOError:
	print("Fichier introuvable/non lisible")
	exit(1)
i = 0

def readInc():
	global i
	global uncompressed_str

	if i >= len(uncompressed_str):
		print("Erreur de compression : fin de la chaîne de caractère inattendue")
		exit(1)
	c = uncompressed_str[i]
	i += 1
	return c
def read(offset):
	global i
	global uncompressed_str

	if i + offset >= len(uncompressed_str):
		print("Erreur de compression : Fin de chaîne inatendue")
		exit(1)
	return uncompressed_str[i + offset]

CHR_MISC         = '000' # 1e : § - 2e : . - 3e : ¨ - 4e : ! - 5e : \0
CHR_SLASH        = '001'
CHR_BACKSLASH    = '010'
CHR_UNDERSCORE   = '011'
CHR_NEW_LIGNE    = '100'
CHR_SPACE        = '101'
CHR_SLASH_SPACE  = '110'
CHR_BSLASH_SPACE = '111'
out = []

miscs = ["§", ".", "¨", "!", "'"]
current_misc = 0
def isValidLetter(c):
	global current_misc
	global miscs

	if c in miscs:
		if current_misc >= len(miscs):
			print("Lettre invalide : toutes les lettres divers ont déjà été utilisées")
		elif miscs[current_misc] == c:
			current_misc += 1
			return True
		else:
			print("Lettre invalide : la lettre divers n'est pas utilisé dans le bon ordre")
		exit(1)
	return False

def printer():
        global out

        while len(out) % 8 != 0:
                out.append(CHR_MISC)

        progOutput = ""

        offset = 0
        while True:
                if offset >= len(out):
                        print(progOutput)
                        exit(0)
                else:
                        current_digit = '0' + out[offset] + '0' + out[offset + 1]
                        progOutput += chr(int(current_digit, 2))
                        offset += 2

while True:
	if  i >= len(uncompressed_str):
		out.append(CHR_MISC)
		printer()
		exit(0)
	c = readInc()
	#print(hex(ord(c)), "'" + c + "'")
	if c == " ":
		out.append(CHR_SPACE)
	elif c == "/":
		if read(0) == ' ':
			i += 1
			out.append(CHR_SLASH_SPACE)
		else:
			out.append(CHR_SLASH)
	elif c == "\\":
		if read(0) == ' ':
			i += 1
			out.append(CHR_BSLASH_SPACE)
		else:
			out.append(CHR_BACKSLASH)
	elif c == "_":
		out.append(CHR_UNDERSCORE)
	elif c == "\n":
		out.append(CHR_NEW_LIGNE)
	elif isValidLetter(c):
		out.append(CHR_MISC)
	else:
		print("Erreur de syntaxe : Caractère '{}' ({}) non encodable".format(c, hex(ord(c))))
		exit(1)
