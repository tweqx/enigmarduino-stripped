import sys

if len(sys.argv) < 2:
	print("Usage : {} <fichier à hexifier>".format(sys.argv[0]))
	print("Convertit un fichier en un string C")
	exit(1)

try:
	in_str = open(sys.argv[1], 'r').read()
except IOError:
	print("Fichier introuvable/illisible")
	exit(1)

out = ""

for c in in_str:
	i = hex(ord(c))[2:]
	if len(i) == 1:
		i = "0" + i
	out += "\\x" + i

print(out)
