#include "src/Moniteur/Moniteur.h"
#include "src/Enigmes/Enigme.h"
#include "src/Enigmes/GestionEEPROM.h"
#include "src/PanneauAdmin.h"

void setup() {
	Moniteur::init();

	randomSeed(analogRead(0) + GestionEEPROM::getResetRandomByte());

	if (GestionEEPROM::isEEPROMvide()) {
		Moniteur::println(F("Initialisation..."));

		GestionEEPROM::resetEnigmes();
		GestionEEPROM::resetScores();
	}

	affichageBanniere();
	Moniteur::println(F("Bienvenue sur EnigmArduino ! Nous avons créé un ensemble d'énigmes, votre but est de toutes les résoudre collectivement en joignant vos forces.\nAfin de bien commencer, vous devez avoir sélectionné l'option \"Nouvelle ligne\" dans la première liste déroulante de cette fenêtre.\nSi vous souhaitez retourner au menu principal, il suffit d'appuyer sur le bouton RESET sur l'arduino ; juste à côté du port USB.\nBonne chance !\n"));
}

void loop() {
	// Affichage des options
	if (Enigme::checkAllFinished())
		Moniteur::println(F("Vous avez déjà gagné ! Pour re-jouer, merci de réinitialiser.\n"));
	
	Enigme::affichage();

	Moniteur::print(F(">> "));	

	// Sélection
	String entree = Moniteur::lectureString();

	switch (entree[0]) {
		// Commandes textuelles
		case 'R':
			Moniteur::println(F("STOP ! Êtes-vous sûr? Cette action ne peut pas être révertie"));
			Moniteur::print(F("[N]/o >> "));
			if (Moniteur::expect(F("o"))) {
				Moniteur::println(F("En cours..."));
				GestionEEPROM::resetEnigmes();
			} else
				Moniteur::println(F("Annulé."));

			Moniteur::print('\n');

			break;
		case 'S': {
			unsigned char winner_count = GestionEEPROM::getWinnerCount();

			Moniteur::println(F("\nTableau des scores :"));

			if (winner_count == 0)
				Moniteur::println(F("  Personne n'a encore réussi à gagner :(\n"));
			else {
				for (unsigned int i = 0; i < winner_count; i++) {
					Moniteur::print(F("  - "));
					Moniteur::println(GestionEEPROM::getWinner(i));
				}

				Moniteur::println(F("\nBravo à eux !\n"));
			}

			break; }
		case 'A':
			Moniteur::println(F("Notre but, c'est de proposer un ensemble d'énigmes sur le thème de l'informatique, des maths... sur la science en général.\nVu que la science c'est cool !    \n\nSi vous souhaitez nous contacter...    \n  XXXXXX (non quand même pas, on va pas vous donner notre nom)\n\nHésitez pas à passer le bonjour, ou bien si vous êtes bloqués/qu'il y a un bug... :( C'est nous les responsables ^^\n"));

			break;
		default:
			if (entree == F("XXXXXXXXXXX"))
				PanneauAdmin::menu();
			else if (Enigme::selection(entree) == false)
				Moniteur::println(F("\nEntrée invalide\n"));

			break;
	}
}

// Décompresse et affiche la banière "EnigmArduino"
void affichageBanniere() {
	ROMString banner = F("\x55\x55\x55\x53\x33\x33\x33\x45\x55\x55\x56\x53\x33\x31\x75\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x35\x55\x55\x55\x55\x55\x33\x45\x55\x55\x65\x13\x33\x32\x13\x33\x33\x55\x33\x55\x33\x33\x33\x55\x33\x33\x33\x33\x33\x33\x55\x55\x67\x55\x53\x33\x35\x17\x75\x33\x53\x35\x53\x35\x53\x33\x33\x35\x53\x33\x33\x34\x55\x55\x65\x33\x17\x56\x33\x55\x12\x61\x26\x33\x55\x12\x63\x33\x55\x33\x35\x51\x75\x65\x57\x51\x75\x32\x23\x72\x17\x27\x21\x72\x17\x53\x35\x21\x75\x33\x52\x45\x55\x65\x13\x32\x66\x12\x66\x66\x61\x36\x66\x12\x36\x12\x36\x66\x65\x05\x57\x77\x21\x17\x05\x77\x05\x57\x77\x72\x17\x77\x23\x72\x45\x51\x33\x33\x33\x12\x66\x66\x13\x61\x33\x35\x56\x66\x16\x61\x66\x66\x55\x35\x55\x77\x23\x27\x23\x33\x72\x33\x33\x72\x37\x23\x72\x37\x23\x33\x33\x24\x55\x23\x33\x33\x32\x12\x32\x12\x32\x12\x32\x12\x33\x66\x12\x32\x62\x32\x62\x32\x61\x33\x31\x02\x33\x37\x21\x36\x21\x33\x31\x21\x33\x33\x12\x13\x12\x13\x12\x13\x12\x13\x33\x33\x14\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x53\x33\x36\x66\x55\x55\x55\x55\x55\x55\x55\x52\x33\x31\x02\x33\x31\x45\x55\x55\x55\x55\x55\x55\x55\x55\x55\x51\x33\x33\x36\x14\x55\x55\x55\x55\x55\x55\x55\x55\x55\x55\x23\x33\x33\x21\x45UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU");

	Moniteur::print('\n');

	int misc_id = 0;
	for (unsigned int i = 0;; i++) {
		uint16_t b = 0;
		if (i & 1)
			b = banner[i / 2] & 0b00001111;
		else
			b = (banner[i / 2] & 0b11110000) >> 4;

		if (b == 0b000) {
			if (misc_id >= 5)
				break;

			// Le ¨ et le § sont traités séparément, car en interne, ils sont encodés sur deux bits, contrairement à .!'
			// De ce fait, les sélectionner en utilisant l'opérateur [] ne fonctionne pas, car il retourne un char
			if (misc_id == 2)
				Moniteur::print(F("¨"));
			else if (misc_id == 0)
				Moniteur::print(F("§"));
			else
				Moniteur::print(F(" . !'")[misc_id]);

			misc_id++;
		} else if (b == 0b001)
			Moniteur::print('/');
		else if (b == 0b010)
			Moniteur::print('\\');
		else if (b == 0b011)
			Moniteur::print('_');
		else if (b == 0b100)
			Moniteur::print('\n');
		else if (b == 0b101)
			Moniteur::print(' ');
		else if (b == 0b110)
			Moniteur::print(F("/ "));
		else if (b == 0b111)
			Moniteur::print(F("\\ "));
	}

	Moniteur::println(F("par XXXXXX & XXXXXX | 2019\n"));
}
