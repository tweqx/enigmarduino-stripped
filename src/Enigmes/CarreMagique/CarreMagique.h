#ifndef ENIGMES_CARREMAGIQUE_H
#define ENIGMES_CARREMAGIQUE_H

#include "../Niveaux.h"

class CarreMagique : public Niveaux {
	public:
		CarreMagique();
		virtual ~CarreMagique();

		bool askNiveau(unsigned int niveau);

	private:
		static constexpr int maxNb = 35;

		int nbLose;
		int resultat;
		int** carre;
		int longueur;

		struct Case { int x, y; };

		void afficherCarre() const;
		void remplirCarre(unsigned int taille);
		bool question(int nbDInconnues, CarreMagique::Case casesCachees[]);
};

#endif
