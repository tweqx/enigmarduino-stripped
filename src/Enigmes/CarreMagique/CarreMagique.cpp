#include "CarreMagique.h"

#include "../../Moniteur/Moniteur.h"

CarreMagique::CarreMagique() {
	// Configuration de l'enigme
	nom = F("Les carrés magiques");
	description = F("Bah c'est des carrés magiques, tout le monde sait ce que c'est... Quoi, pas vous?\n C'est des tableaux de nombres où la somme des cases de n'importe quelle ligne, colonne ou diagonale est la même.\n Y'en a de 3x3, 4x4 et 5x5, la position des cases inconnues varie à chaque fois.\n Si tu les fais pas de tête t'iras en enfer\n Et c'est pas cool l'enfer\n Alors j'y suis jamais allé hein mais on m'a dit que c'était pas cool");
	categorie = F("Mathématiques");
	difficulte = Enigme::Difficulte::Moyen;

	nbr_niveaux = 4;
	niveaux = new Niveaux::Niveau[4];
	niveaux[0] = {
		F("Un carré magique en 3x3 pour se chauffer"),
		F("Bah en fait là c'est easy donc je pense que si tu galères c'est que t'as pas compris.")
	};
	niveaux[1] = {
		F("Carré en 4x4 avec 3 cases manquantes"),
		F("Ça va, tranquille, c'est comme celui d'avant mais avec plus de cases.")
	};
	niveaux[2] = {
		F("Carré en 4x4 avec 4 cases manquantes"),
		F("Bah ouais on va monter un peu la difficulté sinon c'est pas marrant mais bon ça reste tranquillou billou :D")
	};
	niveaux[3] = {
		F("Un pitit carré en 5x5 avec 4 cases manquantes parce que ça mange pas de pain."),
		F("En fait là ce qui peut être relou c'est qu'y'a plus de nombres, si mes calculs sont exacts ça fait à peu près 25 cases.\n En plus l'algorithme a une facheuse tendance à créer des nombres à 3 chiffres.\n Donc ouais celui-là peut être un peu relou comme disent les djeuns.")
	};
	

	message_fin = F("Bravo !\nPour aller plus loin : Pas assez de maths ?\nSi vous voulez, vous pouvez :\n - Démontrez le nombre minimal de cases à donner dans un carré magique 3x3 afin qu'il puisse être résolu d'une seule et unique manière\n - Sur le même principe, démontrer une formule donnant le nombre de cases minimal pour un carré de n x n?");

	// Variables de l'enigme
	nbLose = 0;
}
CarreMagique::~CarreMagique() {}

bool CarreMagique::question(int nbDInconnu, CarreMagique::Case casesCache[]) {
	/** Création des strings des cases du carré **/
	String carreUncomplete[longueur][longueur];

	// On remplit le tableau de String pour afficher le carré
	for (int i = 0; i < longueur; ++i)
		for (int j = 0; j < longueur; ++j)
			carreUncomplete[i][j] = carre[i][j];

	// On cache les nombres à donner
	for (int i = 0; i < nbDInconnu; ++i)
		carreUncomplete[casesCache[i].x][casesCache[i].y] = "?";

	/** Affichage du carré **/
	// Affichage d'une barre verticale en fonction de la taille du tableau
	Moniteur::print(' ');
	switch (longueur) {
		case 3:
			for(int i = 0; i < 22; i++)
				Moniteur::print('-');
			break;
		case 4:
			for(int i = 0; i < 29; i++)
				Moniteur::print('-');
			break;
		case 5:
			for(int i = 0; i < 36; i++)
				Moniteur::print('-');
			break;
	}
	Moniteur::print('\n');

	for(int i = 0; i < longueur; i++) {
		// Affichage d'une ligne
		Moniteur::print(F(" | "));
		for(int j = 0; j < longueur; j++) {
			Moniteur::print(carreUncomplete[i][j]);
			if (carreUncomplete[i][j].length() == 1)
				Moniteur::print(F("   "));
			else if (carreUncomplete[i][j].length() == 2)
				Moniteur::print(F("  "));
			else if (carreUncomplete[i][j].length() == 3)
				Moniteur::print(' ');

			Moniteur::print(F(" | "));
		}
		Moniteur::print('\n');

		// Affichage d'une barre verticale en fonction de la taille du tableau
		Moniteur::print(' ');
		switch (longueur) {
			case 3:
				for(int i = 0; i < 22; i++)
					Moniteur::print('-');
				break;
			case 4:
				for(int i = 0; i < 29; i++)
					Moniteur::print('-');
				break;
			case 5:
				for(int i = 0; i < 36; i++)
					Moniteur::print('-');
				break;
		}
		Moniteur::print('\n');
	}

	/** Lecture des cases cachées**/
	for(int i = 0; i < nbDInconnu; i++) {
		Moniteur::print(F("Entrez la valeur de la case qui se situe dans la "));
		Moniteur::print(casesCache[i].x + 1);

		if (casesCache[i].x == 0)
			Moniteur::print(F("ère"));
		else
			Moniteur::print(F("ème"));

		Moniteur::print(F(" ligne de la "));
		Moniteur::print(casesCache[i].y + 1);

		if (casesCache[i].y == 0)
			Moniteur::print(F("ère"));
		else
			Moniteur::print(F("ème"));
		Moniteur::print(F(" colonne\n>> "));

		// Vérification...
		if (!Moniteur::expect((String)carre[casesCache[i].x][casesCache[i].y])) {
			Moniteur::print(F("Bah non :{"));

			nbLose++;
			if (nbLose > 1) {
				Moniteur::print(F(" (je veux pas te mettre la pression hein mais ça fait quand même la "));
				Moniteur::print(nbLose);
				Moniteur::print(F("ème fois que tu rates. Après moi j'dis ça j'dis rien)"));
			}
			Moniteur::print('\n');

			return false;
		}
	}

	nbLose = 0;
	Moniteur::println(F("Ouais, ça m'a l'air correct :D"));
	return true;
}

bool CarreMagique::askNiveau(unsigned int niveau) {
	// On définit les cases qui seront cachés et que l'utilisateur devra entrer
	Case troisCasesInconnues[3];

	troisCasesInconnues[0].x = 2;
	troisCasesInconnues[0].y = 1;

	troisCasesInconnues[1].x = 0;
	troisCasesInconnues[1].y = 1;

	troisCasesInconnues[2].x = 0;
	troisCasesInconnues[2].y = 0;

	Case quatreCasesInconnues[4];

	quatreCasesInconnues[0].x = 3;
	quatreCasesInconnues[0].y = 2;

	quatreCasesInconnues[1].x = 0;
	quatreCasesInconnues[1].y = 2;

	quatreCasesInconnues[2].x = 0;
	quatreCasesInconnues[2].y = 1;

	quatreCasesInconnues[3].x = 1;
	quatreCasesInconnues[3].y = 3;

	// Démarrage de l'enigme selon le numéro de niveau
	Case* casesInconnues = nullptr;
	unsigned int nbrCasesInconnues = 0;

	switch (niveau) {
		case 0:
			CarreMagique::remplirCarre(3);

			casesInconnues = troisCasesInconnues;
			nbrCasesInconnues = 3;
			break;
		case 1:
			CarreMagique::remplirCarre(4);

			casesInconnues = troisCasesInconnues;
			nbrCasesInconnues = 3;
			break;
		case 2:
			CarreMagique::remplirCarre(4);

			casesInconnues = quatreCasesInconnues;
			nbrCasesInconnues = 4;
			break;
		case 3:
			CarreMagique::remplirCarre(5);

			casesInconnues = quatreCasesInconnues;
			nbrCasesInconnues = 4;
			break;
	}

	bool reussi = CarreMagique::question(nbrCasesInconnues, casesInconnues);

	for (int i = 0; i < longueur; i++)
		delete[] carre[i];
	delete[] carre;

	return reussi;
}


void CarreMagique::remplirCarre(unsigned int taille) {
	/** Remplissage du carré en entier afin qu'il serve de solution **/
	longueur = taille;

	carre = new int*[taille];
	for (unsigned int i = 0; i < taille; i++)
		carre[i] = new int[taille];

	// Génération
	resultat = random(0, 40);

	if (longueur == 3) {
		/** Algorithme pour remplir un carré de 3 de côté : https://aupaysdesmaths.pagesperso-orange.fr/cacr_f01.htm **/

		// Assignement des cases à des pointeurs, c'est plus simple
		int *A = &carre[0][0];
		int *B = &carre[0][1];
		int *C = &carre[0][2];
		int *D = &carre[1][0];
		int *E = &carre[1][1];
		int *F = &carre[1][2];
		int *G = &carre[2][0];
		int *H = &carre[2][1];
		int *I = &carre[2][2];

		// Génération de 3 nombres randoms utilisés pour remplir les cases
		int x = random(0, maxNb);
		int a = random(0, maxNb);
		int b = random(0, maxNb);

		// Assignation des cases
		*A = x + a;
		*B = x - 2*b;
		*C = x + 2*a - b;
		*D = x + 2*a - 2*b;
		*E = x + a - b;
		*F = x;
		*G = x - b;
		*H = x + 2*a;
		*I = x + a - 2*b;
	} else if (longueur == 4) {
		/** Algorithme pour remplir un carré de 4x4 : http://villemin.gerard.free.fr/Wwwgvmm/CarreMag/CMor4Gen.htm **/

		// Assignement des cases à des pointeurs, c'est plus simple
		int *A = &carre[0][0];
		int *B = &carre[0][1];
		int *C = &carre[0][2];
		int *D = &carre[0][3];
		int *E = &carre[1][0];//On assigne chaque case à une lettre c'est plus simple
		int *F = &carre[1][1];
		int *G = &carre[1][2];
		int *H = &carre[1][3];
		int *I = &carre[2][0];
		int *J = &carre[2][1];
		int *K = &carre[2][2];
		int *L = &carre[2][3];
		int *M = &carre[3][0];
		int *N = &carre[3][1];
		int *O = &carre[3][2];
		int *P = &carre[3][3];

		// Génération de 7 nombres randoms utilisés pour remplir les cases
		*A = random(0, maxNb);
		*B = random(0, maxNb);
		*C = random(0, maxNb);
		*E = random(0, maxNb);
		*F = random(0, maxNb);
		*G = random(0, maxNb);
		*I = random(0, maxNb);

		// Assignation des cases
		*M = resultat - (*A + *E + *I);
		*D = resultat - (*A + *B + *C);
		*H = resultat - (*E + *F + *G);
		*J = resultat - (*M + *G + *D);
		*N = resultat - (*B + *F + *J);
		*L = -*I + *F + *G;
		*K = resultat - (*I + *J + *L);
		*O = resultat - (*K + *G + *C);
		*P = resultat - (*L + *H + *D);
	} else if (longueur == 5) {
		/** Algorithme pour remplir un carré de 5x5 : http://villemin.gerard.free.fr/Wwwgvmm/CarreMag/CMordr52.htm **/

		// Assignement des cases à des pointeurs, c'est plus simple
		int *A = &carre[0][0];
		int *B = &carre[0][1];
		int *C = &carre[0][2];
		int *D = &carre[0][3];
		int *O = &carre[0][4];
		int *E = &carre[1][0];
		int *F = &carre[1][1];
		int *G = &carre[1][2];
		int *H = &carre[1][3];
		int *P = &carre[1][4];
		int *I = &carre[2][0];
		int *J = &carre[2][1];
		int *Q = &carre[2][2];
		int *R = &carre[2][3];
		int *S = &carre[2][4];
		int *K = &carre[3][0];
		int *L = &carre[3][1];
		int *T = &carre[3][2];
		int *M = &carre[3][3];
		int *N = &carre[3][4];
		int *U = &carre[4][0];
		int *V = &carre[4][1];
		int *W = &carre[4][2];
		int *X = &carre[4][3];
		int *Y = &carre[4][4];

		// Génération de 7 nombres randoms utilisés pour remplir les cases
		*A = random(0, maxNb);
		*B = random(0, maxNb);
		*C = random(0, maxNb);
		*D = random(0, maxNb);
		*E = random(0, maxNb);
		*F = random(0, maxNb);
		*G = random(0, maxNb);
		*H = random(0, maxNb);
		*I = random(0, maxNb);
		*J = random(0, maxNb);
		*K = random(0, maxNb);
		*L = random(0, maxNb);
		*M = random(0, maxNb);
		*N = random(0, maxNb);

		// Assignation des cases
		*O = resultat - *A - *B - *C - *D;
		*P = resultat - *E - *F - *G - *H;
		*T = resultat - *K - *L - *M - *N;
		*U = resultat - *A - *E - *I - *K;
		*V = resultat - *B - *F - *J - *L;
		*Q = resultat - *H - *L - *U - *O;
		*Y = resultat - *A - *F - *M - *Q;
		*S = resultat - *N - *O - *P - *Y;
		*R = resultat - *I - *J - *Q - *S;
		*X = resultat - *D - *H - *M - *R;
		*W = resultat - *U - *V - *X - *Y;
	}
}
