#ifndef ENIGMES_NIVEAUX_HEADER
#define ENIGMES_NIVEAUX_HEADER

#include "Enigme.h"
#include "GestionEEPROM.h"

class Niveaux : public Enigme, private GestionEEPROM::Stockage {
	public:
		Niveaux();
		virtual ~Niveaux();

		// Méthode actuelle à définir dans les classes enfants
		// Son paramètre 'niveau' est le numéro du niveau, commençant à 0.
		// Cette fonction retourne un bool, correspondant à la réussite du niveau
		virtual bool askNiveau(unsigned int niveau) = 0;

		// Structure représentant un niveau : il a un titre, et une description.
		typedef struct {
			ROMString titre;
			ROMString description;
		} Niveau;
	protected:
		unsigned char nbr_niveaux;
		Niveau* niveaux;
		// Préfixe (qui peut être customisé) qui s'affiche ainsi :
		// <préfixe>1 : <titre>
		//
		// Valeur par défaut : "Niveau "
		ROMString prefixe_titre;
		ROMString message_fin;

	private:
		bool ask();
};

#endif
