#include "GestionEEPROM.h"

#include <EEPROM.h>
#include "../Moniteur/Moniteur.h"

namespace GestionEEPROM {
	unsigned char getResetRandomByte() {
		unsigned char rawByte = EEPROM[GestionEEPROM::resetcount_address];

		EEPROM[GestionEEPROM::resetcount_address]++;
		return (rawByte & 0b11110000) >> 4 | (rawByte & 0b00001111) << 4;
	}

	void resetEnigmes() {
		// Reset des headers...
		for (size_t i = 0; i < headers_size; i++)
			EEPROM[headers_address + i] = 0;
		// Reset des données
		for (size_t i = 0; i < data_size; i++)
			EEPROM[data_address + i] = 0;
		// Reset du flag already_won
		EEPROM[alreadywon_address] = 0;
	}
	void resetScores() {
		for (size_t i = 0; i < scores_size; i++)
			EEPROM[scores_address + i] = 0;
	}
	bool isEEPROMvide() {
		bool is_empty = true;
		for (unsigned int i = 0; i < GestionEEPROM::EEPROM_size; i++)
			if (EEPROM[i] != 0xff) {
				is_empty = false;
				break;
			}

		return is_empty;
	}

	bool isEnigmeCompleted(unsigned int enigme_id) {
		if (enigme_id > nbr_enigmes)
			return false;

		return (EEPROM[headers_address + enigme_id / 8] >> (enigme_id % 8)) & 1;
	}
	void setEnigmeCompleted(unsigned int enigme_id) {
		if (enigme_id > nbr_enigmes)
			return;

		unsigned char b = EEPROM[headers_address + enigme_id / 8];
		b |= 1 << (enigme_id % 8);
		EEPROM[headers_address + enigme_id / 8] = b;
	}
	void setEnigmeUncompleted(unsigned int enigme_id) {
		if (enigme_id > nbr_enigmes)
			return;

		unsigned char b = EEPROM[headers_address + enigme_id / 8];
		b &= ~(1 << (enigme_id % 8));
		EEPROM[headers_address + enigme_id / 8] = b;
	}

	bool hasAlreadyWon() {
		return (bool)EEPROM[alreadywon_address];
	}
	void addWinner(String nom) {
		EEPROM[alreadywon_address] = 1; // On indique qu'il y a eu un vainqueur à cette partie

		unsigned char nbr_nom = EEPROM[scores_address];
		if (nbr_nom == 0xff) // Pour éviter l'overflow
			return;

		uintptr_t scores_ptr = scores_address + 1;

		// Avancement du pointeur jusqu'à la fin des noms
		for (unsigned char i = 0; i < nbr_nom; i++) {
			if (scores_ptr > scores_address + 512)
				return;

			// On avance le pointeur jusqu'au null du c-string
			while (EEPROM[scores_ptr++] != 0);
		}

		// Écriture du nom
		for (unsigned char i = 0; i < nom.length(); i++) {
			if (scores_ptr > scores_address + 512)
				return;

			EEPROM[scores_ptr++] = nom[i];
		}
		EEPROM[scores_ptr + 1] = 0;

		EEPROM[scores_address] = nbr_nom + 1;
	}
	String getWinner(unsigned char index) {
		String winner = "";

		if (index >= EEPROM[scores_address])
			// Pas sûr que ça soit la meilleur solution de retourner un string contenant l'erreur - en même temps, c'est pratique à débugger, et ça passe pour un nom d'équipe bizarre
			return F("Index invalide").toString();

		uintptr_t scores_ptr = scores_address + 1;

		// Avancement du pointeur jusqu'au nom
		for (unsigned char i = 0; i < (signed)index; i++) {
			if (scores_ptr > scores_address + 512)
				return F("Sortie de la section SCORES").toString();

			// On avance le pointeur jusqu'au null du c-string
			while (EEPROM[scores_ptr++] != 0)
				if (scores_ptr > scores_address + 512)
					return F("Sortie de la section SCORES").toString();
		}

		// Lecture du nom
		for (unsigned char i = 0; EEPROM[scores_ptr + i] != 0; i++) {
			if (i >= 50)
				return F("Null-byte probablement manquant").toString();

			winner += (char)EEPROM[scores_ptr + i];
		}

		return winner;
	}
	unsigned char getWinnerCount() {
		return EEPROM[scores_address];
	}

	unsigned int Stockage::data_offset = data_address;
	Stockage::Stockage(unsigned short taille_donnees_persistantes) : addresse(Stockage::data_offset), data_size(taille_donnees_persistantes) {
		Stockage::data_offset += this->data_size;
	}
	void Stockage::saveData(void* in_data) {
		unsigned char* address = static_cast<unsigned char*>(in_data);

		for (unsigned int i = 0; i < this->data_size; i++)
			EEPROM[this->addresse + i] = address[i];
	}
	void Stockage::getData(void* out_data) {
		unsigned char* address = static_cast<unsigned char*>(out_data);

		for (unsigned int i = 0; i < this->data_size; i++)
			address[i] = EEPROM[this->addresse + i];
	}
}
