#ifndef CHASSE_AU_TRESOR_H
#define CHASSE_AU_TRESOR_H

#include "../Enigme.h"

class ChasseAuTresor : public Enigme {
	public:
		ChasseAuTresor();
		~ChasseAuTresor();

		bool ask();
};

#endif
