#include "ChasseAuTresor.h"
#include "../../Moniteur/Moniteur.h"

ChasseAuTresor::ChasseAuTresor() {
	nom = F("Chasse au trésor");
	description = F("Une chasse au trésor mythique pour trouver un trésor légendaire !! \n Bon en vrai c'est quand même pas aussi incroyable. \n Mais c'est pas mal !!");
	categorie = F("IRL (ça veut dire \"Dans la vraie vie\" en espagnol)");

	difficulte = Enigme::Difficulte::Facile;
}
ChasseAuTresor::~ChasseAuTresor() {}

bool ChasseAuTresor::ask() {
	Moniteur::println(F(" Yo jeune aventurier solitaire, arriveras-tu à résoudre la sombre énigme de la carte au trésor d'OSI ? \n Toi seul le peux, seras-tu suffisament brave ? \n L'avenir nous le dira ... \n \n Bon pour commencer, tu dois aller aux coordonnées GPS suivantes : 44.540542, 5.168036.\n Ensuite, déplacez-vous vers le sud jusqu'à trouver une citerne, près d'une maison en taule. Notez son poids en kilo.\n À l'ouest, vous trouverez sur une dalle. Relevez le nombre de crochets rouillés sur cette dalle.\n Revenez à l'auditorium, et suivez le sentier à sa droite qui monte vers des cabanes en bois. Explorez jusqu'à trouver deux trapes en fer. Relevez le nombre inscrit sur celle ayant une cheminée pointue.\n Redescendez à l'auditorium. De là, cherchez la boule en pierre se situant en hauteur et rejoingnez-la. Comptez le nombre de poteaux que vous voyez à l'horizon (Attention, ils sont bien cachés!)\n À proximité d'un ancien mur d'escalade, il y a un tuyau souterrain en béton. Combien de segments comporte-t-il ?\n Près du bâtiment où se trouve les salles d'activités et les chambres, il y a un escalier en colimaçon. Combien de marche comporte-t-il ?\n\n\n Ok, vous avez les réponses? :D"));

	Moniteur::lectureString();

	// Poids de la citerne : 1550 kg
	// Crochets : 3
	// Nombre sur la trappe en fer : 600
	// Nombre de poteaux électriques : 2
	// Nombre de segments du tuyau : 4
	// Nombre de marches de l'escalier : 16/17/18
	Moniteur::println(F("Le poids de la citerne ?"));
	Moniteur::print(F(">> "));
	if (Moniteur::lectureNombre() != 1550)
		goto echec;

	Moniteur::println(F("Le nombre de crochets ?"));
	Moniteur::print(F(">> "));
	if (Moniteur::lectureNombre() != 3)
		goto echec;

	Moniteur::println(F("Le nombre sur la trappe en fer ?"));
	Moniteur::print(F(">> "));
	if (Moniteur::lectureNombre() != 600)
		goto echec;

	Moniteur::println(F("Le nombre de poteaux électriques ?"));
	Moniteur::print(F(">> "));
	if (Moniteur::lectureNombre() != 2)
		goto echec;

	Moniteur::println(F("Le nombre de segments du tuyau ?"));
	Moniteur::print(F(">> "));
	if (Moniteur::lectureNombre() != 4)
		goto echec;

	Moniteur::println(F("Le nombre de marches de l'escalier ?"));
	Moniteur::print(F(">> "));
	int nbr_marches = Moniteur::lectureNombre();
	if (nbr_marches != 16 && nbr_marches != 17 && nbr_marches != 18)
		goto echec;

	Moniteur::println(F("Bravo !\nVoici votre récompense : www.bit.ly/bravoGIF"));
	return true;

	echec:
	Moniteur::println(F("Ahh non désolé..."));
	return false;
}
