#include "Enigme.h"

#include <EEPROM.h>
#include <avr/boot.h>

#include "../Moniteur/Moniteur.h"
#include "GestionEEPROM.h"

Enigme::Enigme() {}
Enigme::~Enigme() {}

// Affiche la liste des énigmes ainsi :
//
// Liste des énigmes disponibles :
//  1 |  Les portes logiques
//  2 |  Carrés Magiques - Complété!
//  3 |  Chasse au trésor
//
//  S |  Tableau des scores
//  R |  Réinitialisation de la progression
//  A |  À propos...
// >>
void Enigme::affichage() {
		Moniteur::println(F("Liste des énigmes disponibles :"));

		// Pas de message spécial s'il n'y a aucune énigme, pour sauver de la ROM - et pas vraiment utile
		for (unsigned int i = 0; i < Enigme::nbr_enigmes; i++) {
			const Enigme& enigme = *Enigme::enigmes[i];

			Moniteur::print(' ');
			Moniteur::print(i + 1);
			Moniteur::print(F(" |  ")); // Espace à ajuster en fonction du nombre d'énigmes
			Moniteur::print(enigme.nom);

			if (GestionEEPROM::isEnigmeCompleted(i))
				Moniteur::print(F(" - Complétée"));

			Moniteur::print('\n');
		}
		Moniteur::print('\n');
		Moniteur::println(F(" S |  Tableau des scores\n R |  Réinitialisation de la progression\n A |  À propos..."));
}

bool Enigme::selection(String entree) {
	unsigned int selection = entree.toInt();

	if (selection > 0 && selection <= Enigme::nbr_enigmes) {
		Enigme& enigme = *Enigme::enigmes[selection - 1];

		Moniteur::println(F("=========="));

		// Affichage des informations à propos de l'énigme sous le format : (exemple)
		// Énigme n°N: Titre court
		//
		// Catégorie : un ou deux nom de catégorie - Difficulté : [***]
		// Description : description de quelques lignes
		//
		Moniteur::print(F("\nÉnigme n°"));
		Moniteur::print(selection);
		Moniteur::print(F(" : "));
		Moniteur::print(enigme.nom);
		if (GestionEEPROM::isEnigmeCompleted(selection - 1))
			Moniteur::print(F(" - Déjà complétée"));
		Moniteur::print('\n');


		Moniteur::print(F("\nCatégorie : "));
		Moniteur::print(enigme.categorie);
		Moniteur::print(F(" - Difficulté : "));
		if (enigme.difficulte == Enigme::Difficulte::Facile)
			Moniteur::println(F("[*  ]\n"));
		else if (enigme.difficulte == Enigme::Difficulte::Moyen)
			Moniteur::println(F("[** ]\n"));
		else
			Moniteur::println(F("[***]\n"));

		Moniteur::print(F("Description : "));
		Moniteur::println(enigme.description);

		Moniteur::println(F("\nReady ?"));
		Moniteur::lectureString();

		if (enigme.ask()) {
			GestionEEPROM::setEnigmeCompleted(selection - 1);
		}

		Moniteur::println(F("\n=========="));

		return true;
	}

	return false;
}

bool Enigme::checkAllFinished() {
	for (unsigned int i = 0; i < Enigme::nbr_enigmes; i++)
		if (!GestionEEPROM::isEnigmeCompleted(i))
			return false;

	if (GestionEEPROM::hasAlreadyWon())
		return true;

	Moniteur::print(F("Félicitations ! Vous avez complété l'ensemble des énigmes !\nConcertez-vous, trouvez un nom d'équipe, pour entrer dans l'histoire d'EnigmArduino... On vous écoute !\n>> "));
	lectureNom:
	String nom = Moniteur::lectureString();

	if (nom.length() == 0) {
		Moniteur::print(F("Ça fait un peu court, un nom de 0 caractères... Réessayez.\n>> "));
		goto lectureNom;
	} else if (nom.length() > 30) {
		Moniteur::print(F("Hé, il faut laisser de la place pour les suivants ! Moins de 30 caractères svp.\n>> "));
		goto lectureNom;
	}

	GestionEEPROM::addWinner(nom);
	return true;
}

// Pour ajouter une énigme, il faut :
// 1°) Ajouter '#include "chemin/vers/le.h"' juste après ce commentaire
// 2°) Incrémenter nbr_enigmes dans Enigme.h
// 3°) Ajouter dans le tableau ci-dessous 'new <TypeEnigme>', avec éventuellement des paramètres.

#include "PortesLogiques/PortesLogiques.h"
#include "CarreMagique/CarreMagique.h"
#include "ChasseAuTresor/ChasseAuTresor.h"
#include "Trivia/Trivia.h"
#include "QCMeduses/QCMeduses.h"

Enigme* Enigme::enigmes[Enigme::nbr_enigmes] = {
	new PortesLogiques,
	new CarreMagique,
	new ChasseAuTresor,
	Trivia::qcm,
	QCMeduses::qcm
};

