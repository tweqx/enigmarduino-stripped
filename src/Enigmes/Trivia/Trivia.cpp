#include "Trivia.h"

/** Les Strings **/

#define GID 0

// Infos sur l'énigme
GD("Trivia", 0);
GD("Des questions sur la science !", 1);
GD("Sciences", 2);

// Catégories
GD("Épistémologie", 3);
GD("Biologie", 4);
GD("Mathématiques", 5);
GD("Physique", 6);
GD("Histoire", 7);
GD("Informatique", 8);

// Questions
GD("Tout d'abord, que veut dire le titre de ce domaine ?", 9);
GD("Laquelle de ces sciences n'existe pas ?", 10);
GD("Quelle recherche scientifique sur les chats n'a jamais été publiée ?", 11);
GD("Quel est le poison le plus puissant au monde ?", 12);
GD("Dans quel milieu autre que l'atmosphère pouvons-nous respirer?", 13);
GD("Quelle taille fait l'organisme unicellulaire le plus grand jamais découvert ?", 14);
GD("Si l'arithmétique correspond à l'étude des nombres et que la géométrie correspond à l'étude de figures dans le plan, quel est le nom de la branche des mathématiques qui étudie la nature même des objets ?", 15);
GD("Quel est le nom du symbole de l'infini?", 16);
GD("Sur quel site de discussion un utilisateur anonyme a-t-il résolu un problème posé il y a plusieurs dizaines d'années par des mathématiciens en voulant trouver le meilleur ordre pour regarder des épisodes d'un anime ?", 17);
GD("Chaque cyclone a un prénom ; par exemple, Irma. Quelle condition leurs noms ne doivent pas remplir ?", 18);
GD("En connaissant avec une précision infinie la position, la vitesse, etc... de toutes les particules de l'univers à un instant T, que ne pourrait-t-on pas prévoir ?", 19);
GD("Laquelle de ces événements insolites n'a pas eu lieu pendant la seconde guerre mondiale ?", 20);
GD("D'où provenait le fer qui a été utilisé pour fabriquer une dague qui a été retrouvé dans le tombeau de Toutânkhamon ?", 21);
GD("Quel est le mot clé le plus recherché sur le moteur de recherche Bing ?", 22);
GD("99% des communications d'Internet sont transmises entre les continents par des câbles sous l'océan. Combien de ces câbles existe-t-il actuellement, et quelle longueur mis bout-à-bout forment-ils?", 23);
GD("En pouvant tester 1000 mots de passe par seconde, combien de temps en moyenne on mettrait pour trouver un mot de passe de 20 caractères ?", 24);

// Explications
GD("Le B : en fait, c'est une science qui a pour but d'étudier la connaissance scientifique en général, et son unité.\n"
"Par exemple, ils tentent de trouver la meilleure méthode pour enseigner les sciences, ou bien ils réfléchissent à la méthode scientifique.\n"
"On a choisi ce mot comme titre de cette section, où on parlera de sciences insolites :)", 25);
GD("C'est la suspontologie... La fouloscopie n'est pas une science très connue, elle n'est en fait que très récente.\"
"Ça consiste en l'étude du comportement d'une foule : l'effet de groupe, par exemple.", 26);
GD("C'est la D : Une étude a bien été publié dans le but de montrer que les chats sont liquides. En vérité, c'était plus une blague... et c'est une étude française en plus !\n"
"Concernant la couleur de pelage et l'aggressivité d'un chat, une étude statisque a montré que certains types de pelage sont associés à une aggressivité.\n"
"Mais attention, cela reste à prouver... Le parasite de la réponse C s'appelle la toxoplasmose, et il est démontré qu'il modifie le comportement de rats, en leur enlevant la peur des chats.\n"
"C'est flippant :D", 27);
GD("C'est la toxine botulique, qui est produite par une bactérie présente dans la terre.\n"
"Les mesures effectuées révèlent qu'il suffit en moyenne de 0.4 nanogrammes de cette toxine pour tuer un animal d'un kilogramme.\n"
"Pour se donner une idée de sa puissance, il suffirait d'une tasse de café pour anéantir toute la population humaine !", 28);
GD("C'est le perfluorocarbure : oui, ça peut sembler incroyable, mais c'est possible de respirer dans un liquide.\n"
"Vous avez déjà vu le film Abyss? La scène où un rat est forcé de respirer dans un liquide a été réalisée sans trucages !\n"
"Cependant, le rat est mort lorsqu'il est sorti du perfluorocarbure pour respirer dans l'air.", 29);
GD("C'est le D: c'est une espèce qui a été découverte au large des côtes d'Écosse.\n"
"On ne sait pas vraiment comment cette espèce se reproduit ni se nourrit...\n"
"On connait plus les petites bactéries que les gros organisme unicellulaires... x)", 30);
GD("Réponse : la topologie ! Ce domaine des mathématiques reste éloigné de la géométrie parce que lorsqu'on étudie la topologie d'un objet, il est possible de le déformer n'importe comment, du moment qu'on ne le \"coupe\" pas.\n"
"Ainsi, on a droit de dire que topologiquement, une tasse et un beignet sont identiques : ils ont tout deux un trou !", 31);
GD("C'est le lemniscate.", 32);
GD("C'est 4chan! Incroyable, non? Le pire, c'est que cette personne anonyme n'a jamais été retrouvée...", 33);
GD("C'est la D ; les cyclones peuvent avoir un prénom de plus de 7 caractères ^^\"
"Par contre ces règles ne sont pas identiques dans toutes les régions du monde : dans l'océan indien, c'est un autre organisme qui décide du nom des cyclones, par exemple.\n"
"Autre chose à noter : lorsqu'un cyclone fait beaucoup de morts, on le supprime de la liste, pour éviter de réveiller certains souvenirs douloureux...", 34);
GD("De façon étonnante, ça serait votre ombre ! En fait, tout les événements à grande échelle, tels qu'un lancé de dé, sont prévisibles en connaissant les conditions de départ.\n"
"Cependant, pour votre ombre, les particules de lumière la compose (ou plutôt, qui ne la compose pas :p) sont régies par les lois de la physique quantique, or elle est complètement aléatoire.", 35);
GD("Le tournoi de football a bien eu lieu, en 1942. Le match le plus connu de ce tournoi a été nommé le \"match de la mort\".\n"
"Une équipe ukrainienne a remporté la victoire contre une équipe de nazis, mais le match s'est déroulé dans le fair-play.\n"
"C'est après la guerre que la propagande soviétique a repris cet évènement, faisant des joueurs ukrainiens des héros ayant résistés aux nazis, donnant au match son surmon.\n"
"Wojtek, l'ours-soldat était un caporal de l'armée polonaise, il transportait les munitions sur le champ de bataille.\n"
"Il est devenu l'emblème de la division qu'il accompagnait : elle a officiellement été changée en celle d'un ours transportant un obus.\n"
"C'est pendant les derniers jours de la seconde guerre mondiale que des soldats allemands se sont alliés à des soldats américains pour déloger des SS d'un château qui refusait la fin de la guerre.", 36);
GD("C'est bien du fer de météorite ! C'est la grande quantité de nickel présente dans la dague qui permis de confirmer l'origine extra-terrestre du fer.", 37);
GD("C'est 'google'... cocasse, non?\n"
"On remarque 'how to get help in windows 10' est en 5ème position, avec 'youtube' et 'facebook' en seconde et en troisième position.\n"
"Quand même, je me demande pourquoi Microsoft continue de faire fonctionner Bing si leurs premiers résultats de recherches sont pour des sites concurents x)", 38);
GD("Réponse B: il y en a 378, pour une longueur de 1.2 million de km.\n"
"Le plus court fait 131 km, le plus long 20000 km : il relie l'Asie à l'Amérique.", 39);
GD("Réponse : A! En plus dans mon calcul, j'ai considéré que le mot de passe contenait uniquement l'alphabet en minuscule et en majuscule, + les chiffres...\n"
"Imaginez qu'on aurait utilisé des caractères spéciaux, et qu'on ne connaitrait pas à l'avance la longueur du mot de passe... :O", 40);

// Réponses
GD("L'étude des maladies", 41);
GD("La science de la science", 42);
GD("Le classement de la biodiversité sur Terre", 43);
GD("Les sciences occultes", 44);

GD("La fouloscopie, la science des foules", 45);
GD("L'oenologie, la science du vin", 46);
GD("L'entomologie, l'étude des insectes", 47);
GD("La suspontologie, science des ponts et structures en hauteur", 48);

GD("Les chats peuvent être considérés comme des liquides", 49);
GD("Il y aurait un lien entre la couleur de pelage et l'aggressivité d'un chat", 50);
GD("Un parasite, se servant de chats comme hôte, peut infecter d'autres espèces et modifier leur comportement", 51);
GD("Les chats préfèrent les poissons d'eau douces que les poissons marins", 52);

GD("La toxine botulique, secrétée par une bactérie", 53);
GD("Le cyanure", 54);
GD("Le venin de l'araignée de Sydney", 55);
GD("Le Zyklon B", 56);

GD("L'eau oxygénée", 57);
GD("Le perfluorocarbure", 58);
GD("L'eau sous une très forte pression", 59);
GD("L'ozone, de formule chimique 03 (avec 02 la formule chimique de l'oxygène)", 60);

GD("0.75 mm", 61);
GD("Quelques centimètres", 62);
GD("10 cm", 63);
GD("20 cm", 64);

GD("L'analyse", 65);
GD("La théorie des groupes", 66);
GD("La topologie", 67);
GD("Il n'y a pas de différences entre la géométrie et l'étude de la forme d'objets", 68);

GD("la clepsydre", 69);
GD("zêta", 70);
GD("le lemniscate", 71);
GD("sigma", 72);

GD("Discord", 73);
GD("4chan", 74);
GD("Reddit", 75);
GD("Facebook", 76);

GD("Il y a une alternance des genres des prénoms", 77);
GD("Il n'y a jamais de prénoms en U, Q, X, Y ou Z", 78);
GD("Des listes pré-établies de prénoms se répètent tout les 6 ans", 79);
GD("Les prénoms ont toujours une longueur inférieure à 7 caractères", 80);

GD("Le résultat d'un lancé de dé", 81);
GD("La météo de demain", 82);
GD("Votre ombre", 83);
GD("La date de mort naturelle du président", 84);

GD("Les américains et les allemands se battant contre les nazis", 85);
GD("Un tournoi de match de football organisé par les nazis lors de leur occupation en Ukraine", 86);
GD("Un ours a participé à la seconde guerre mondiale", 87);
GD("Un contructeur automobile forcé de collaborer fabriquait des chars allemands piégés", 88);

GD("Du fer météoritique, provenant de météorites", 89);
GD("De carières de fer en Mésopotamie", 90);
GD("De le pyrite de fer trouvée dans les rivières", 91);
GD("De fer importé d'Europe", 92);

GD("'yt'", 93);
GD("'google'", 94);
GD("'facebook'", 95);
GD("'how to get help in windows 10'", 96);

GD("Il y en a 28, pour une longueur de 700 000 km", 97);
GD("Il y en a 378, pour une longueur de 1.2 million de km", 98);
GD("Il y en a 139, pour une longueur de 1.2 million de km", 99);
GD("Il y en a 532, pour une longueur de 700 000 km", 100);

GD("~500 millions de milliards de milliards d'années", 101);
GD("~5000 ans", 102);
GD("~500 ans", 103);
GD("~5 ans", 104);

// Message de fin
GD("Merci d'avoir fait ce quizz :D \n J'espère que t'auras appris des choses... Pour allez plus loin, tu peux checker les chaînes youtube ci-dessous; c'est toujours cool d'apprendre des choses ne serait-ce que pour avoir des annecdotes sympas à raconter ;)\n\n100SekundenPhysik : Physique [Allemand]\n3Blue1Brown : Mathématiques [Anglais]\nAbsol Vidéos : Documentaires, réflexions, explications... très chouette :D [Français]\nBen Eater : Informatique et électronique [Anglais]\nCyrus North : Philo [Français]\nDeus Ex Silicium : Électronique [Français]\nDirtyBiology : Biologie & Sciences Sociales (Psychologie...) [Français]\nDr Nozman : Biologie, Géologie... [Français]\ne-penser : Psychologie, Physique, Biologie, Philosophie... [Français]\nEl Jj : Mathématiques [Français]\nEpic Teaching of History : Histoire [Français]\nEt tout le monde s'en fout : Philosophie, Réflexions... [Français]\nFouloscopie : J'espère que vous avez bien suivi le quizz... héhé... [Français]\nHuman Trail : Biologie, Botanique [Français, ancien moniteur d'OSI!!!]\nHygiène Mentale : Philosophie, Réflexion.... [Français]\nIt's Okay To Be Smart : Physique, Biologie [Anglais]\nLa statistique expliquée à mon chat : Sociologie, Mathématiques [Français]\nLe Vortex : Collectifs de vulgarisateurs scientifiques [Français]\nLeiosOS : Physique, Maths, Informatique [Anglais]\nLinguisticae : Histoire, Études des langues [Français]\nLiveOverflow : Informatique [Anglais]\nMamytwink : Histoire [Français]\nMathologer : Mathématiques [Anglais]\nMickaël Launay : Mathématiques [Français]\nMicode : Informatique [Français]\nminutephysics : Physique [Anglais]\nNileRed : Chimie [Anglais]\nPhysicsGirl : Physique [Anglais]\nPrimer : Biologie, Statistique [Anglais]\nPwnFunction : Informatique [Anglais]\nScienceEtonnante : Physique, Mathématique [Français]\nSmarterEveryDay : Biologie, Physique [Anglais]\nThe 8-Bit Guy : Informatique [Anglais]\nThe Thought Emporium : Biologie, Physique [Anglais]\nThink Twice : Mathématiques [Anglais]\nTu mourras moins bête : Biologie, Physique... [Anglais]\nVsauce : Physique, Biologie [Anglais]\n", 105);

/** Les structures de données **/
QCM::Quizz::Categorie::Question epistemologie_questions[] = {
	{
		G(9), //# Tout d'abord, que veut dire le titre de ce domaine ?

		{
			G(41),
			G(42),
			G(43),
			G(44),
		},

		QCM::ReponseValide::B,

		G(25)
	},
	{
		G(10), //# Laquelle de ces sciences n'existe pas ?

		{
			G(45),
			G(46),
			G(47),
			G(48),
		},

		QCM::ReponseValide::D,

		G(26)
	},
	{
		G(11), //# Quelle recherche scientifique sur les chats n'a jamais été publiée ?

		{
			G(49),
			G(50),
			G(51),
			G(52),
		},

		QCM::ReponseValide::D,

		G(27)
	}
};
QCM::Quizz::Categorie::Question biologie_questions[] = {
	{
		G(12), //# Quel est le poison le plus puissant au monde ?

		{
			G(53),
			G(54),
			G(55),
			G(56)
		},

		QCM::ReponseValide::A,

		G(28)
	},
	{
		G(13), //# Dans quel milieu autre que l'atmosphère pouvons-nous respirer?

		{
			G(57),
			G(58),
			G(59),
			G(60)
		},

		QCM::ReponseValide::B,

		G(29)
	},
	{
		G(14), //# Quelle taille fait l'organisme unicellulaire le plus grand jamais découvert ?

		{
			G(61),
			G(62),
			G(63),
			G(64)
		},

		QCM::ReponseValide::D,

		G(30)
	}
};
QCM::Quizz::Categorie::Question mathematiques_questions[] = {
	{
		G(15), //# Si l'arithmétique correspond à l'étude des nombres et que la géométrie correspond à l'étude de figures dans le plan, quel est le nom de la branche des mathématiques qui étudie la nature même des objets ?

		{
			G(65),
			G(66),
			G(67),
			G(68)
		},

		QCM::ReponseValide::C,

		G(34)
	},
	{
		G(16), //# Quel est le nom du symbole de l'infini?

		{
			G(69),
			G(70),
			G(71),
			G(72)
		},

		QCM::ReponseValide::C,

		G(35)
	},
	{
		G(17), //# Sur quel site de discussion un utilisateur anonyme a-t-il résolu un problème posé il y a plusieurs dizaines d'années par des mathématiciens en voulant trouver le meilleur ordre pour regarder des épisodes d'un anime ?

		{
			G(73),
			G(74),
			G(75),
			G(76)
		},

		QCM::ReponseValide::B,

		G(36)
	}
};
QCM::Quizz::Categorie::Question physique_questions[] = {
	{
		G(18), //# Chaque cyclone a un prénom ; par exemple, Irma. Quelle condition leurs noms ne doivent pas remplir ?

		{
			G(77),
			G(78),
			G(79),
			G(80)
		},

		QCM::ReponseValide::D,

		G(34)
	},
	{
		G(19), //# En connaissant avec une précision infinie la position, la vitesse, etc... de toutes les particules de l'univers à un instant T, que ne pourrait-t-on pas prévoir ?

		{
			G(81),
			G(82),
			G(83),
			G(84)
		},

		QCM::ReponseValide::C,

		G(35)
	}
};
QCM::Quizz::Categorie::Question histoire_questions[] = {
	{
		G(20), //# Laquelle de ces événements insolites n'a pas eu lieu pendant la seconde guerre mondiale ?

		{
			G(85),
			G(86),
			G(87),
			G(88),
		},

		QCM::ReponseValide::D,

		G(36)
	},
	{
		G(21), //# Laquelle de ces événements insolites n'a pas eu lieu pendant la seconde guerre mondiale ?

		{
			G(89),
			G(90),
			G(91),
			G(92)
		},

		QCM::ReponseValide::A,

		G(37)
	}
};
QCM::Quizz::Categorie::Question informatique_questions[] = {
	{
		G(22), //# Quel est le mot clé le plus recherché sur le moteur de recherche Bing ?

		{
			G(93),
			G(94),
			G(95),
			G(96)
		},

		QCM::ReponseValide::B,

		G(38)
	},
	{
		G(23), //# 99% des communications d'Internet sont transmises entre les continents par des câbles sous l'océan. Combien de ces câbles existe-t-il actuellement, et quelle longueur mis bout-à-bout forment-ils?

		{
			G(97),
			G(98),
			G(99),
			G(100)
		},

		QCM::ReponseValide::B,

		G(39)
	},
	{
		G(24), //# En pouvant tester 1000 mots de passe par seconde, combien de temps en moyenne on mettrait pour trouver un mot de passe de 20 caractères ?

		{
			G(101),
			G(102),
			G(103),
			G(104)
		},

		QCM::ReponseValide::A,

		G(40)
	}
};

QCM::Quizz::Categorie categories[] = {
	{
		G(3), //# Épistémologie

		3,
		epistemologie_questions
	},
	{
		G(4), //# Biologie

		3,
		biologie_questions
	},
	{
		G(5), //# Mathématiques

		3,
		mathematiques_questions
	},
	{
		G(6), //# Physique

		2,
		physique_questions
	},
	{
		G(7), //# Histoire

		2,
		histoire_questions
	},
	{
		G(8), //# Informatique

		3,
		informatique_questions
	}
};

namespace Trivia {
	QCM::Quizz quizz = {
		// Infos sur l'énigme
		G(0),
		G(1),
		G(2),
		G(105),
		Enigme::Difficulte::Moyen,

		// Catégories
		6,
		categories
	};

	QCM* qcm = new QCM(quizz);
}

