#include "QCMeduses.h"

#include "../../Utils/ROMString.h"

#define GID 1

/** Les Strings **/

// Infos sur l'énigme
GD("Le quizz sur les méduses", 0);
GD(" Les méduses sont des animaux fascinants...\n Avant d'écrire ce quizz je pensais que c'était des vieux trucs mous et gélatineux, mais en vrai ceux sont des bestioles impressionnantes !\n Bon, pas celles qui te piquent sur les plages.\n Mais certaines sont incroyables !\n Prêt pour découvrir le monde fabuleux des méduses ?\n Chaque question sera constitué d'une suite d'affirmations. Trois sont vraies, une est fausse ; à vous de la repérer. Bonne chance !\n\n/!\\ IMPORTANT : Lisez bien après avoir répondu à une question afin de savoir quelle affirmation était fausse !! (Histoire de pas apprendre des trucs faux...).\n", 1);
GD("Biologie", 2);
GD("GG !", 40);

// Catégorie
GD("Méduses", 3);

// Question
GD("HEY ! Je sais pas si vous savez mais ...", 38);

// Réponses
GD("Les méduses sont des espèces plus vieilles que les dinosaures", 4);
GD("Leur bouche est aussi... leur anus", 5);
GD("Elles se propulsent en crachant par leur bouche", 6);
GD("Certaines vont à plus de 20km/h", 7);

GD("Les méduses digérent rapidement, sinon elles deviennent trop lourdes pour flotter", 9);
GD("Des fossiles ont prouvé que des méduses chassaient les dinosaures", 10);
GD("Les méduses sont présentes dans tous les océans du monde", 11);
GD("Certaines méduses vivent dans l'eau douce", 12);

GD("Certaines méduses sont immortelles", 14);
GD("Certaines piqûres de méduses sont mortelles pour l'homme", 15);
GD("Certaines méduses ont des poils", 16);
GD("Certaines méduses sont à peine visibles à l'oeil nu", 17);

GD("Il existe des méduses sans tentacules, qui se nourrissent en fonctionnant comme des gros sacs poubelles, en emprisonnant \"en\" elles leurs proies", 18);
GD("Des méduses fluorescentes peuvent être utilisées pour un usage médical", 19);
GD("Il existe une méduse ayant des marques colorées ressemblant au logo de nike", 20);
GD("Des méduses ont déjà pu empêcher le fonctionnement de centrales nucléaires en bouchant le tuyau de refroidissement", 21);

GD("Des chercheurs ont prouvé que certaines méduses sont sensibles à la musique et au rythme en général", 22);
GD("Des méduses n'ont pas de cerveau mais juste des réseaux nerveux qui détectent les changements de l’environnement et coordonnent les actions", 23);
GD("Les plus gros rassemblements de méduses sont constitués de 100 000 individus", 24);
GD("Les méduses peuvent se reproduire aussi avec des spermatozoïdes et des ovules qu'en se clonant", 25);

GD("Elles sont commestibles : en Asie, elles se dégustent grillées !", 26);
GD("Vu que les méduses peuvent se reproduire de façon asexuée, il y a des colonies de méduses d'un seul sexe : mâle ou femelle", 27);
GD("Les méduses peuvent se nourrir de beurre de cacahuète, l'apport de protéine est suffisant pour elles", 28);
GD("Il a été prouvé que les méduses sont sensibles aux émotions humaines", 29);

GD("Les méduses pourraient prendre le contrôle des océans : elles se multiplient à une vitesse alarmante selon les chercheurs", 30);
GD("On peut charger un téléphone android en mettant une méduse sur la prise de chargement", 31);
GD("Certaines méduses mangent d'autres méduses", 32);
GD("La poudre de méduse peut être utilisé pour faire du caramel au beurre salé", 33);

GD("Apparemment, les méduses pourraient \"sentir\" leur proies dans l'eau grâce aux substances chimiques que celles-ci libèrent", 34);
GD("Une méduse a 24 capteurs oculaires, sortes d'yeux, dont 4 sont pointés vers le haut afin de pouvoir se diriger dans les mangroves", 35);
GD("Les méduses doivent vivre dans des aquariums ronds sinon elles peuvent se coincer dans les coins et mourir", 36);
GD("Les méduses sont une source d'énergie qui remplacera peut être le nucléaire !!", 37);

// Explications
GD("Les méduses dépassent rarement les 6km/h", 8);
GD("Les méduses étant molles, on retrouve peu de fossiles.", 13);

// Divers
GD("", 39);

/** Les structures de données **/
QCM::Quizz::Categorie::Question meduses_questions[] = {
	{
		G(38),
		{
			G(4),
			G(5),
			G(6),
			G(7)
		},
		QCM::ReponseValide::D,
		G(8)
	},
	{
		G(38),
		{
			G(9),
			G(10),
			G(11),
			G(12)
		},
		QCM::ReponseValide::B,
		G(13)
	},
	{
		G(38),
		{
			G(14),
			G(15),
			G(16),
			G(17)
		},
		QCM::ReponseValide::C,
		G(39)
	},
	{
		G(38),
		{
			G(18),
			G(19),
			G(20),
			G(21)
		},
		QCM::ReponseValide::C,
		G(39),
	},
	{
		G(38),

		{

			G(22),
			G(23),
			G(24),
			G(25)
		},

		QCM::ReponseValide::A,
		G(39)
	},
	{
		G(38),

		{
			G(26),
			G(27),
			G(28),
			G(29)
		},

		QCM::ReponseValide::D,
		G(39)
	},
	{
		G(38),
		{
			G(30),
			G(31),
			G(32),
			G(33)
		},
		QCM::ReponseValide::B,
		G(39)
	},
	{
		G(38),
		{
			G(34),
			G(35),
			G(36),
			G(37)
		},
		QCM::ReponseValide::D,
		G(39)
	}
};

QCM::Quizz::Categorie categories[] = {
	{
		G(3),

		8,
		meduses_questions
	}
};

QCM::Quizz quizz = {
	// Infos sur l'énigme
	G(0),
	G(1),
	G(2),
	G(40),
	Enigme::Difficulte::Facile,

	// Catégories
	1,
	categories
};

namespace QCMeduses {
	QCM* qcm = new QCM(quizz);
}

