Epreuve 12 : Les portes logiques

Catégorie : Électronique/Logique | Niveau de difficulté : [***]

Description : Ça vous dit d'apprendre comment un PC marche ? Hé, ne fuyez pas, c'est plus simple qu'il n'y paraît :)
Au cœur du fonctionnement des ordinateurs, il y a des portes logiques, vous en avez peut-être déjà entendu parler.
Ce sont des composants électroniques dont leur sortie dépend de l'état des entrées du composant.
Il existe deux états dans un circuit logique : l'état Haut/1, c'est le 5V ; l'état Bas/0, c'est le 0V.

À travers ces 10 niveaux, vous devez prédire le résultat d'un circuit logique que l'on vous donne.
Il y a toujours les lettres de A à H dans ces circuits : elles correspondent chacune à une broche de l'arduino.
 A = 2    B = 3    C = 4    D = 5
 E = 6    F = 7    G = 8    H = 6

On vous impose l'état de A B C et D, vous devez les lire sur l'arduino, à l'aide de LEDs ou d'un multimètre par exemple.
C'est à vous de répondre à l'arduino avec l'état de E F G et H, qu'il faut prédire et câbler.

Même s'il suffit en théorie de "suivre les fils" pour résoudre chaque niveau, c'est toujours sympa de
prendre le temps de comprendre ce que fait précisément le circuit logique dans son ensemble :
On ne s'est pas amusé à mettre des portes logiques dans tous les sens pour voir ce que ça fait ^^

Circuit logique n°1 : Simple & Basique

Description : Allez, pour se chauffer !
Il faut bien faire la distinction entre deux fils se croisant et se coupant :

Par exemple :

            .   .-----                   .   .-----
            '---|---.                    '---+---.
           -----'   '                   -----'   '

      Les lignes se croisent        Les lignes se coupent

Schéma :

    A -------------------------------------- E

    B -------------------------------------- F

    C -------.   .-------------------------- G
             '---|---.
    D -----------'   '---------------------- H

Circuit logique n°2 : La porte NON

Description : La porte NON est une porte logique dont la sortie est l'inverse de l'entrée.
Elle est schématisée par "--[NON]--".

Schéma :

    A -----------------[NON]---------------- E

    B -------------------------------------- F

    C --------[NON]--------------[NON]------ G

    D -------------------------------------- H

Circuit logique n°3 : La porte ET

Description : Encore une nouvelle porte logique !
Celle-ci, c'est la porte ET : elle a deux entrées et une sortie.
Sa sortie ne s'active uniquement si les deux entrées sont à l'état haut (la première ET la seconde entrée doivent être à l'état haut... Elle porte bien son nom, non? :P)

Schéma :

    A                                .------ E
                                     |
    B ---------------[====]          +------ F
                     | ET |----------+
    C ---------------[====]          +------ G
                                     |
    D                                '------ H

Circuit logique n°4 : Combinaison secrète

Description : Enfin un peu d'action :)
Quand une sortie n'est pas connectée, on considère qu'elle est à l'état bas.

Schéma :

    A -------------[====]               .--- E
                   | ET |               |
    B ----[NON]----[====]----[====]-----'    F
                             | ET |
    C -------------[====]----[====]          G
                   | ET |
    D ----[NON]----[====]                    H

Circuit logique n°5 : La porte OU

Description : Sa sortie s'active quand l'une OU l'autre de ses entrées est à l'état haut...
Mais attention, c'est un 'ou' inclusif : si les deux entrées sont à l'état haut, la sortie sera quand même à l'état haut.

Schéma :

           .--------.-----------------.
    A -----'        |                 '----- E
             .------'---------------.
    B -------'                      '------- F
              .---------------.
    C --------|               '------------- G
              '-----[====]
    D ------.       | OU |------------------ H
            '-------[====]

Circuit logique n°6 : Des bases moins basiques

Description : Ha Ah ! Vous connaissez les différentes bases ? Mais si, la fameuse blague "Il y a 10 types de personnes.
Ceux qui connaissent le binaire, et ceux qui ne le connaissent pas".
Si vous ne connaissez pas cela, allez vous renseigner, c'est très important ici ! :D

Schéma :

    A        .-----[====]       .----------- E
             |     | OU |-------'
    B -------'  .--[====]          .-------- F
                |                  |
    C -------------[====]          |         G
                |  | OU |----------'
    D ----------'--[====]                    H

Circuit logique n°7 : Et dans l'autre sens ?

Description : Tout est dans le titre, c'est sur le même principe ;)
Astuce : n'hésitez pas à recopier le schéma sur du papier

Schéma :
                 .---.----[NON]-[====]
                 |   |          | ET |--.
    A -----------'   |  +-[NON]-[====]  '--- E
                     +------------[====]
    B -----------.   |  |         | ET |---- F
                 '------+-[NON]---[====]
    C                |  +-------[====]  .--- G
                     |  |       | ET |--'
    D                +----[NON]-[====] .---- H
                     '--|------[====]  |
                        |      | ET |--'
                        '------[====]

Circuit logique n°8 : La porte XOR

Description : Ne vous inquiétez pas, c'est la dernière porte, promis !
Son nom vient de la traduction anglaise de "OU exclusif" : "eXclusive OR".
Le principe est simple : la sortie est à l'état haut quand UNE et UNE SEULE entrée est à l'état haut.

Schéma :
             .----------[====]
             |          | ET |---.
             |  .-[NON]-[====]   '--[====]
    A -------+----.                 | OU |-- E
             |  | |              .--[====]
    B ---.------+ '-[NON]-[====] |           F
         |   |  |         | ET |-'
    C    |   |  '---------[====]             G
         |   '---[=====]
    D    |       | XOR |-------------------- H
         '-------[=====]

Circuit logique n°9 : C'est juste une addition, ok...?

Description : Ça commence à se compliquer, hein?
Encore une fois, on vous conseille de le recopier.

Schéma :

       .----+-------------------[=====]
       | .----+---------------. | XOR |-.
    A -' |  | '--[====]       '-[=====] '--- E
         |  |    | ET |-.----------[=====]
    B -. |  '----[====] |          | XOR |-- F
       +-|---[=====]    | .--------[=====]
    C ---'   | XOR |------'-[====]        .- G
       |  .--[=====]    |   | ET |-[====] |
    D ----'-[====]      '---[====] | OU |-'  H
       |    | ET |-----------------[====]
       '----[====]

Circuit logique n°10 : L'univers a cessé de fonctionner

Description : Ok, celui là est juste pour la blague xD

Schéma :

    A           [1]--[====]                  E
                     | ET |-----.
    B             .--[====]     |   .------- F
                  '-------------|-.-'
    C              .----[NON]---' |          G
                   '-[====]       |
    D                | ET |-[NON]-'          H
                [1]--[====]

Message de fin : Bravo ! Vous êtes maintenant devenu un maître en logique... Respect.
C'était les bases du fonctionnement d'un ordinateur : mais bien sûr, c'est énooormément plus complexe dans la réalité.
N'empêche que c'est toujours nos amis les portes NON, les portes ET, les portes OU et les portes XOR qui sont les "blocs" élémentaires des ordinateurs.
