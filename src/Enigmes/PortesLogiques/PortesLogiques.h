#ifndef ENIGMES_PORTESLOGIQUES_H
#define ENIGMES_PORTESLOGIQUES_H

#include "../Niveaux.h"

class PortesLogiques : public Niveaux {
	public:
		PortesLogiques();
		~PortesLogiques();

		bool askNiveau(unsigned int niveau);

		void printSchema(const ROMString& compressed);
};

#endif
