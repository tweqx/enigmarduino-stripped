#include "QCM.h"

#include "../../Utils/ROMString.h"
#include "../GestionEEPROM.h"
#include "../../Moniteur/Moniteur.h"

QCM::QCM(QCM::Quizz q) : GestionEEPROM::Stockage(sizeof(QCM::DataEEPROM)) {
	// Configuration de l'énigme
	this->categorie = q.nom_categorie;
	this->description = q.description;
	this->nom = q.titre;
	this->difficulte = q.difficulte;

	this->quizz = q;
}
QCM::~QCM() {}

bool QCM::ask() {
	QCM::DataEEPROM avancement;
	this->getData(&avancement);

	while (true) {
		QCM::Quizz::Categorie& categorie = this->quizz.categories[avancement.nbr_categorie];

		// Categorie
		Moniteur::print(F("== "));
		Moniteur::print(categorie.nom);
		Moniteur::println(F(" ==\n"));

		// Question
		QCM::Quizz::Categorie::Question question = categorie.questions[avancement.nbr_question];
		Moniteur::println(question.question);

		// Réponses
		Moniteur::print(F(" A: "));
		Moniteur::println(question.reponses.A);
		Moniteur::print(F(" B: "));
		Moniteur::println(question.reponses.B);
		Moniteur::print(F(" C: "));
		Moniteur::println(question.reponses.C);
		Moniteur::print(F(" D: "));
		Moniteur::println(question.reponses.D);

		// Vérification de la réponse
		verification:

		Moniteur::print(F(">> "));
		String reponse_str = Moniteur::lectureString();

		QCM::ReponseValide reponse;
		if (reponse_str == F("A"))
			reponse = QCM::ReponseValide::A;
		else if (reponse_str == F("B"))
			reponse = QCM::ReponseValide::B;
		else if (reponse_str == F("C"))
			reponse = QCM::ReponseValide::C;
		else if (reponse_str == F("D"))
			reponse = QCM::ReponseValide::D;
		else {
			Moniteur::println(F("Pardon ? Merci d'entrez A, B, C ou D :)"));
			goto verification;
		}

		if (reponse != question.reponse_valide)
			Moniteur::println(F("Mauvaise réponse !\n"));
		else {
			Moniteur::println(F("Bonne réponse !\n"));
			Moniteur::println(question.explication);

			// Incrémentation de l'avancement
			avancement.nbr_question++;
			if (avancement.nbr_question >= categorie.nbr_questions) {
				avancement.nbr_question = 0;
				avancement.nbr_categorie++;

				// Fin de l'épreuve, on reset l'avancement pour les suivants, et on quitte
				if (avancement.nbr_categorie >= this->quizz.nbr_categories) {
					avancement.nbr_categorie = 0;
					avancement.nbr_question = 0;
					this->saveData(&avancement);

					// Message de fin
					Moniteur::println(this->quizz.message_fin);

					return true;
				}
			}
			// Sauvegarde
			this->saveData(&avancement);
		}
	}
}
