#ifndef ENIGMES_QCM_HEADER
#define ENIGMES_QCM_HEADER

#include "../../Utils/ROMString.h"
#include "../Enigme.h"
#include "../GestionEEPROM.h"

class QCM : public Enigme, GestionEEPROM::Stockage {
	public:
		enum class ReponseValide {
			A, B, C, D
		};

		typedef struct {
			ROMString titre;
			ROMString description;
			ROMString nom_categorie;
			ROMString message_fin;
			Enigme::Difficulte difficulte;

			unsigned char nbr_categories;
			struct Categorie {
				ROMString nom;

				unsigned int nbr_questions;
				struct Question {
					ROMString question;

					struct Reponses {
						ROMString A;
						ROMString B;
						ROMString C;
						ROMString D;
					} reponses;

					QCM::ReponseValide reponse_valide;

					ROMString explication;
				};

				Question* questions;
			};

			Categorie* categories;
		} Quizz;

		QCM(QCM::Quizz quizz);
		~QCM();

		bool ask();

		typedef struct {
			unsigned int nbr_categorie;
			unsigned int nbr_question;
		} DataEEPROM;

	private:
		QCM::Quizz quizz;
};

#endif
