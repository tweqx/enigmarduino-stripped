#ifndef ENIGMES_H_INCLUDED
#define ENIGMES_H_INCLUDED

#include "../Utils/ROMString.h"

class Enigme {
	public:
		Enigme();
		virtual ~Enigme();

		enum class Difficulte {
			Facile,   // [*  ]
			Moyen,    // [** ]
			Difficile // [***]
		};

		static void affichage();
		static bool selection(String entree);
	protected:
		virtual bool ask() = 0;

		ROMString nom;
		ROMString description;
		ROMString categorie;
		Difficulte difficulte;

	public:
		static bool checkAllFinished();

		static constexpr unsigned int nbr_enigmes = 5;
		static Enigme* enigmes[nbr_enigmes];
};

#endif
