#ifndef ENIGMES_GESTIONEEPROM_H
#define ENIGMES_GESTIONEEPROM_H

#include <stdint.h> // pour uintptr_t
#include <stdlib.h> // pour size_t
#include <avr/io.h> // pour E2END
#include <WString.h>

#include "Enigme.h"

namespace GestionEEPROM {
	// Organisation de l'EEPROM :
	//
	// HEADERS:
	//   Espace de stockage situé en début de l'EEPROM, qui permet de sauvegarder si les énigmes sont complétées.
	//   L'état de complétition de chaque énigme est stocké comme un flag dans 1 bit ; ainsi, on peut en stocker 8 par octets.
	// DATA:
	//   Espace de stockage désigné au stockage divers et varié des données des énigmes. Il est situé juste après les headers.
	//   Chaque enigme déclare la quantité de mémoire dont elle nécessite, et une addresse lui est assignée.
	// ALREADY_WON:
	//   Flag sur un octet qui permet de déterminer si les joueurs ont déjà gagnés sur la partie actuelle (= depuis le reset des énigmes)
	// SCORES:
	//   Zone situé en fin d'EEPROM, elle permet de stocker les noms des équipes et des personnes qui ont réussi
	//   à compléter les énigmes
	//
	// +----------+  <- headers_address: 0
	// | HEADERS  |
	// |- - - - - |  <- data_address
	// | DATA     |
	// |          |
	//==============
	// |        ##|  <- resetcount_address + alreadywon_address
	// | - - - - -|  <- scores_address
	// | SCORES   |
	// |          |
	// +----------+  <- EEPROM_size - 1
	constexpr size_t EEPROM_size = E2END + 1;

	constexpr size_t nbr_enigmes = Enigme::nbr_enigmes;

	constexpr uintptr_t headers_address = 0;
	constexpr size_t headers_size = Enigme::nbr_enigmes / 8 + 1;
	constexpr size_t resetcount_size = 1;
	constexpr size_t alreadywon_size = 1;
	constexpr uintptr_t data_address = headers_address + headers_size;
	constexpr uintptr_t scores_size = 512;
	constexpr size_t data_size = EEPROM_size - scores_size - resetcount_size - alreadywon_size - data_address;
	constexpr uintptr_t resetcount_address = data_address + data_size;
	constexpr uintptr_t alreadywon_address = resetcount_address + resetcount_size;
	constexpr size_t scores_address = EEPROM_size - scores_size;

	// getResetRandomByte()
	unsigned char getResetRandomByte();

	// resetEnigmes() & resetScores(), isEEPROMvide()
	// Deux fonctions permettant de réinitaliser les énigmes et les scores, ainsi qu'une fonction retournant true si l'EEPROM est vide (=> remplie de 0xff)
	// Comme les premiers octets de l'EEPROM sont toujours utilisés, cette fonction détecte rapidement si l'EEPROM n'est pas vide
	void resetEnigmes();
	void resetScores();
	bool isEEPROMvide();

	// isEnigmeCompleted() & setEnigmeCompleted()/setEnigmeUncompleted()
	// Permet de retourner l'état de complétition d'une énigme, et de déclarer une énigme comme complétée/non complétée
	bool isEnigmeCompleted(unsigned int enigme_id);
	void setEnigmeCompleted(unsigned int enigme_id);
	void setEnigmeUncompleted(unsigned int enigme_id);

	// hasAlreadyWon() & addWinner() & getWinner() & getWinnerCount()
	// Respectivement, retourne true ou false en fonction de si les joueurs ont déjà gagné cette partie (pour éviter de re-demander plusieurs fois le score)
	//  et ajoute une équipe gagnante
	// getWinner() retourne sous forme d'un String le nom de l'équipe gagnante à un index, et getWinnerCount() donne le nombre de gagnants enregistrés
	bool hasAlreadyWon();
	void addWinner(String nom);
	String getWinner(unsigned char index);
	unsigned char getWinnerCount();

	// Classe Stockage, qui facilite l'accès & l'attribution de la mémoire de la partie DATA de l'EEPROM.
	//
	// Utilisée correctement, elle a une propriété addresse, contenant l'addresse dans l'EEPROM attribuée à Enigme.
	// On peut soit écrire directement dans l'EEPROM en utilisant la bibliothèque arduino du même nom, soit utiliser les fonctions
	//  saveData() et getData()
	class Stockage {
		protected:
			Stockage(unsigned short taille_donnees_persistantes);
			~Stockage() = default;

			void saveData(void* in_data);
			void getData(void* out_data);

			const unsigned int addresse;
			const unsigned short data_size;

		private:
			static unsigned int data_offset;
	};
}

#endif
