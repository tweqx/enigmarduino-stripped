#include "Niveaux.h"

#include "../Moniteur/Moniteur.h"

Niveaux::Niveaux() : Stockage(sizeof(unsigned char)) {
	// Pour éviter l'erreur "statement-expressions are not allowed outside functions"
	prefixe_titre = F("Niveau ");
}
Niveaux::~Niveaux() {}

bool Niveaux::ask() {
	unsigned char numero_niveau;
	this->getData(&numero_niveau);

	while (true) {
		const Niveaux::Niveau& niveau = this->niveaux[numero_niveau];

		// Affchage du n° de l'énigme + du titre + de la description
		Moniteur::print(F("~~~~~~~~~~~~~~~\n+ "));
		Moniteur::print(this->prefixe_titre);
		Moniteur::print(numero_niveau + 1);
		if (niveau.titre.length()) {
			Moniteur::print(F(" : "));
			Moniteur::println(niveau.titre);
		}
		if (niveau.description.length()) {
			Moniteur::print(F("\n+ Description : "));
			Moniteur::println(niveau.description);
		}
		Moniteur::print('\n');

		// si l'utilisateur échoue le niveau, on quitte l'énigme
		if (this->askNiveau(numero_niveau) == false)
			return false;

		// Sinon, on passe au niveau suivant & on met dans l'EEPROM qu'elle a été complétée
		numero_niveau++;
		this->saveData(&numero_niveau);

		Moniteur::print('\n');

		if (numero_niveau >= this->nbr_niveaux) {
			Moniteur::println(this->message_fin);
			numero_niveau = 0; // Réinitialisation des niveaux dans l'EEPROM, permet de permettre de re-jouer à l'énigme même si elle est complétée
			this->saveData(&numero_niveau);
			return true;
		}
	}
}
