#include "PanneauAdmin.h"

namespace PanneauAdmin {
	void menu() {
		// Panneau admin
		Moniteur::print('\n');
		Moniteur::println(F("Panneau administrateur - Commandes valides :"));
		Moniteur::println(F("  R |  Réinitialisation des scores"));
		Moniteur::println(F("  C |  Forcer à compléter/décompléter une énigme"));
		Moniteur::println(F("  W |  Ajouter un gagnant"));
		Moniteur::println(F("  E |  Editer et voir la mémoire"));

		while (true) {
			Moniteur::print(F(">> "));
			String entree = Moniteur::lectureString();

			// Commandes
			switch (entree[0]) {
				case 'R':
					GestionEEPROM::resetScores();

					break;
				case 'C': {
					Moniteur::print(F("N° de l'énigme à compléter/décompléter (index commençant à 0)? "));
					int enigme = Moniteur::lectureNombre();

					if (GestionEEPROM::isEnigmeCompleted(enigme))
						GestionEEPROM::setEnigmeUncompleted(enigme);
					else
						GestionEEPROM::setEnigmeCompleted(enigme);

					break; }
				case 'W':
					Moniteur::print(F("Entrez le nom du gagnant : "));

					GestionEEPROM::addWinner(Moniteur::lectureString());

					break;
				case 'E':
					PanneauAdmin::Options::editeurHexa();

					break;
				default:
					Moniteur::println(F("Commande invalide"));
					break;
			}

			Moniteur::print('\n');
		}
	}

	namespace Options {
		void editeurHexa() {
			Moniteur::println(F("Utilisez les touches 'q' et 'd' pour déplacer la sélection de la mémoire, 'z' et 's' pour changer se déplacer dans la mémoire.\nEnvoyez 'c' pour quitter, et 'e' si vous souhaitez passer de navigation entre les mémoires à modification des mémoires ou inversement.\nLorsque vous êtes en mode 'Édition', la touche 'm' permet de modifier la valeur sélectionnée.\n"));

			bool mode_edition = false;
			unsigned long edition_addresse = 0;
			unsigned long addresses[3] = {0};
			unsigned char memoire_selectionnee = 0;

			for (;;) {
				// C'est plus optimisé de séparer les différentes parties de la ligne, car le compilateur va utiliser la même addresse
				//  pour stocker les grands espaces
				Moniteur::print(F("PROGMEM"));
				Moniteur::print(F("                                            "));
				Moniteur::print(F("SRAM"));
				Moniteur::print(F("                                            "));
				Moniteur::println(F("   EEPROM"));

				Moniteur::print(F("+------------------------------------------+"));
				Moniteur::print(F("       "));
				Moniteur::print(F("+------------------------------------------+"));
				Moniteur::print(F("       "));
				Moniteur::println(F("+------------------------------------------+"));

				// Addresses
				for (unsigned int i = 0; i < 3; i++) {
					Moniteur::print(F("| 0x"));
					Moniteur::printHex(addresses[i]);
					if (i != memoire_selectionnee)
						Moniteur::print(F(" :                             |       "));
					else {
						Moniteur::print(F(" :  <="));
						Moniteur::print(F("                         |       "));
					}
				}
				Moniteur::print('\n');

				// Dump
				for (unsigned int i = 0; i < 16; i++) {
					for (unsigned int k = 0; k < 3; k++) {
						if (mode_edition && k == memoire_selectionnee && edition_addresse == addresses[k] + i * 8)
							Moniteur::print(F("|  ["));
						else
							Moniteur::print(F("|   "));

						// Dump en hexa
						for (unsigned int j = 0; j < 8; j++) {
							unsigned char b;

							if (k == 0)
								b = pgm_read_byte(addresses[0] + i * 8 + j);
							else if (k == 1)
								b = *(unsigned char*)(addresses[1] + i * 8 + j);
							else
								b = EEPROM[addresses[2] + i * 8 + j];

							Moniteur::printHex(b);
							if (mode_edition && k == memoire_selectionnee) {
								if (edition_addresse == addresses[k] + i * 8 + j)
									Moniteur::print(']');
								else if (edition_addresse % 8 != 0 && edition_addresse == addresses[k] + i * 8 + j + 1)
									Moniteur::print('[');
								else
									Moniteur::print(' ');
							} else
								Moniteur::print(' ');
						}
						Moniteur::print(F(": "));

						// Dump en ascii (filtré)
						for (unsigned int j = 0; j < 8; j++) {
							unsigned char b;

							if (k == 0)
								b = pgm_read_byte(addresses[0] + i * 8 + j);
							else if (k == 1)
								b = *(unsigned char*)(addresses[1] + i * 8 + j);
							else
								b = EEPROM[addresses[2] + i * 8 + j];

							if (b >= ' ' && b <= '~')
								Moniteur::print((char)b);
							else
								Moniteur::print('.');
						}
						Moniteur::print(F("     |       "));
					}
					Moniteur::print('\n');
				}

				Moniteur::print(F("+------------------------------------------+"));
				Moniteur::print(F("       "));
				Moniteur::print(F("+------------------------------------------+"));
				Moniteur::print(F("       "));
				Moniteur::println(F("+------------------------------------------+"));

				String action = Moniteur::lectureString();
				if (action[0] == 'C' || action[0] == 'c')
					break;
				else if (action[0] == 'Q' || action[0] == 'q') {
					if (!mode_edition)
						memoire_selectionnee = memoire_selectionnee == 0 ? 2 : memoire_selectionnee - 1;
					else if (edition_addresse != 0) {
						edition_addresse--;
						addresses[memoire_selectionnee] = edition_addresse - (edition_addresse % (16 * 8));
					}
				}
				else if (action[0] == 'D' || action[0] == 'd') {
					if (!mode_edition)
						memoire_selectionnee = memoire_selectionnee == 2 ? 0 : memoire_selectionnee + 1;
					else {
						edition_addresse++;
						addresses[memoire_selectionnee] = edition_addresse - (edition_addresse % (16 * 8));
					}
				}
				else if (action[0] == 'Z' || action[0] == 'z') {
					if (!mode_edition) {
						if (addresses[memoire_selectionnee] < 16*8)
							addresses[memoire_selectionnee] = 0;
						else
							addresses[memoire_selectionnee] -= 16*8;
					} else if (edition_addresse > 7)
							edition_addresse -= 8;
				}
				else if (action[0] == 'S' || action[0] == 's') {
					// On check pas que l'addresse ne dépasse pas la taille maximale de la mémoire,
					//  car ça complexifirait tout (utiliser des constantes spécifiques à chaque version d'arduino, etc...)
					//  et au pire, les valeurs lues corresponderont à l'addresse modulo taille de la mémoire.

					if (!mode_edition)
						addresses[memoire_selectionnee] += 16*8;
					else
						edition_addresse += 8;
				} else if (action[0] == 'E' || action[0] == 'e') {
					if (memoire_selectionnee == 0)
						Moniteur::println(F("La mémoire PROGMEM/Flash n'est pas éditable !"));
					else {
						mode_edition = !mode_edition;

						if (mode_edition)
							edition_addresse = addresses[memoire_selectionnee];
					}
				} else if (mode_edition && (action[0] == 'm' || action[0] == 'M')) {
					Moniteur::print(F("Entrez la valeur en hexadécimal à remplacer (sans ou avec '0x') : "));
					unsigned char valeur_remplacement = Moniteur::lectureNombreHex(sizeof(unsigned char));

					if (memoire_selectionnee == 1)
						*(unsigned char*)edition_addresse = valeur_remplacement;
					else
						EEPROM[edition_addresse] = valeur_remplacement;
				}
			}
		}
	}
}
