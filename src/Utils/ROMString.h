#ifndef STRING_HEADER
#define STRING_HEADER

#include <Arduino.h>

// Classe visant à simplifier l'accès aux Strings de ROM
// Des instances à cette classe ne peuvent être uniquement récupérés via la macro F, cf. ci-dessous
class ROMString {
	public:
		ROMString() = default;
		ROMString(char*); // Ce constructeur ne doit pas être appellé !

		unsigned int length() const;
		uintptr_t addresseROM() const;
		char operator[](unsigned int i) const;

		bool operator==(const String& str) const;
		bool operator==(int val) const;
		bool operator!=(const String& str) const;
		bool operator!=(int val) const;

		String toString() const;

	private:
		void* address;
};

bool operator==(const String& str, const ROMString& other_str);
bool operator==(int val, const ROMString& str);
bool operator!=(const String& str, const ROMString& other_str);
bool operator!=(int val, const ROMString& str);

/* Macros F */
#undef F
#undef PSTR

// La variable dummy est utilisé pour empêcher l'optimisation de PSTR : elle est déclarée volatile
// De plus, le '(void)dummy' est là pour faire croire au compilateur que dummy est utilisé, ce qui permet de supprimer un warning
#define PSTR(str) \
	(__extension__({ \
		PGM_P ptr;  \
		asm volatile \
		( \
			".pushsection .progmem.fstr, \"SM\", @progbits, 1" "\n\t" \
			"0: .string " #str								 "\n\t" \
			".popsection"									  "\n\t" \
		); \
		asm volatile \
		( \
			"ldi %A0, lo8(0b)"								 "\n\t" \
			"ldi %B0, hi8(0b)"								 "\n\t" \
			: "=d" (ptr) \
		); \
		volatile int dummy = 0; \
		(void)dummy; \
		ptr; \
	}))
#define F(string_literal) (static_cast<ROMString>(const_cast<char*>(PSTR(string_literal))))

#define GD(str, id) template<unsigned int i>\
__attribute__((always_inline)) inline ROMString globalString_ ## id () { \
	return F(str); \
}
#define G(id) globalString_ ## id <GID>()

#endif
