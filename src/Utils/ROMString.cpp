#include "ROMString.h"

#include <avr/pgmspace.h>
#include <stdint.h>

ROMString::ROMString(char* addr) {
	address = addr;
}

unsigned int ROMString::length() const {
	unsigned int length = 0;
	uintptr_t addr = addresseROM();

	if (!addr)
		return 0;

	while (true) {
		char c = pgm_read_byte(addr);

		if (c == 0)
			return length;
		length++;
		addr++;
	}
}

uintptr_t ROMString::addresseROM() const {
	return (uintptr_t)address;
}

char ROMString::operator[](unsigned int i) const {
	if (!address)
		return 0;

	if (i > length() - 1)
		return 0;

	return pgm_read_byte((uintptr_t)address + i);
}

bool ROMString::operator==(const String& str) const {
	if (str.length() != length())
		return false;

	for (unsigned int i = 0; i < str.length(); i++)
		if (str[i] != this->operator[](i))
			return false;
	return true;
}
bool ROMString::operator==(int val) const {
	return this->operator==(String(val));
}
bool ROMString::operator!=(const String& str) const {
	return !this->operator==(str);
}
bool ROMString::operator!=(int val) const {
	return !this->operator==(val);
}

String ROMString::toString() const {
	return String(reinterpret_cast<const __FlashStringHelper*>(address));
}

bool operator==(const String& str, const ROMString& other_str) {
	return other_str.operator==(str);
}
bool operator==(int val, const ROMString& str) {
	return str.operator==(val);
}
bool operator!=(const String& str, const ROMString& other_str) {
	return other_str.operator!=(str);
}
bool operator!=(int val, const ROMString& str) {
	return str.operator!=(val);
}
