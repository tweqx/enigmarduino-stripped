#include "ListeOption.h"

namespace Moniteur {
	void liste(const ListeOption liste[], const unsigned int& nbr_elements, void* argument_generique = nullptr) {
		for (;;) {
			for (unsigned int i = 0; i < nbr_elements; i++)
				println("[" + String(i) + "] " + liste[i].message);

			print("> ");
			int choix = lectureNombre();

			if (choix >= 0 && choix <= (signed)nbr_elements - 1) { // le 'signed' est là pour supprimer le warning
				liste[choix].selection(argument_generique);
				break;
			}

			println(PSTR("[!] Choix invalide"));
		}
	}
}
