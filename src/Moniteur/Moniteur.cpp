#include "Moniteur.h"

#include <Arduino.h>
#include <limits.h> // Pour ULONG_MAX

#include "../Utils/ROMString.h"

namespace Moniteur {
	void init() {
		Serial.begin(Moniteur::BaudRate);
		Serial.setTimeout(ULONG_MAX); // Configuration du timeout pour tout ce qui est entrée de l'utilisateur, ici le nombre maximal possible est ULONG_MAX
	}

	template<>
	void print<ROMString>(const ROMString& arg) {
		unsigned int i = 0;
		while (true) {
			char c = arg[i];

			if (c == 0) break;
			print(c);

			i++;
		}
	}
	template<>
	void print<char>(const char& c) {
		Serial.write(c);
	}

	int lectureNombre() {
		int lecture = Serial.parseInt();
		println(lecture);
		// On flush le port série : on jette tous les caractères de l'utilisateur
		// Ça permet d'éviter des désagréments, car lorsqu'un utilisateur envoie un nombre,
		//  par exemple 100, un retour à la ligne sera aussi envoyé.
		// Mais Serial.parseInt() extrait que le nombre, et pas le retour à la ligne, laissant
		//  ainsi le retour à la ligne dans le buffer. On le supprime en flushant le buffer :)
		while (Serial.available()) Serial.read();

		return lecture;
	}
	String lectureString() {
		String lecture = Serial.readStringUntil('\n');
		println(lecture);

		// On flush le port série pour les même raisons que ci-dessus
		while (Serial.available()) Serial.read();

		return lecture;
	}
	void attendreEntree() {
		Serial.readStringUntil('\n');

		while (Serial.available()) Serial.read();
	}

	unsigned long long lectureNombreHex(unsigned int nbr_octets) {
		re_ask:
		unsigned long long valeur_lue = 0;
		int hexa_offset = 0;
		String str_hex = Moniteur::lectureString();

		if (str_hex.length() > nbr_octets * 2 + 2) {
			Moniteur::println(F("Saisie invalide (trop longue)\n"));
			goto re_ask;
		}
		if (str_hex[0] == '0' && str_hex[1] == 'x')
			hexa_offset = 2;
		if (str_hex.length() - hexa_offset > nbr_octets * 2) {
			Moniteur::println(F("Saisie invalide (trop longue)\n"));
			goto re_ask;
		}

		unsigned char shift = 0;
		for (int i = str_hex.length() - 1; i >= hexa_offset; i--) {
			if (str_hex[i] >= '0' && str_hex[i] <= '9')
				valeur_lue |= static_cast<unsigned long long>((str_hex[i] - '0') & 0xf) << shift;
			else if (str_hex[i] >= 'a' && str_hex[i] <= 'f')
				valeur_lue |= static_cast<unsigned long long>((str_hex[i] - 'a' + 10) & 0xf) << shift;
			else if (str_hex[i] >= 'A' && str_hex[i] <= 'F')
				valeur_lue |= static_cast<unsigned long long>((str_hex[i] - 'A' + 10) & 0xf) << shift;
			else {
				Moniteur::println(F("Caractère non hexadécimal trouvé !"));
				goto re_ask;
			}

			shift +=4;
		}

		return valeur_lue;
	}


	bool expect(const String& entree) {
		return lectureString() == entree;
	}
	bool expect(const ROMString& entree) {
		return lectureString() == entree;
	}
}
