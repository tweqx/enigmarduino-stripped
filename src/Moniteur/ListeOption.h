#ifndef MONITEUR_LISTEOPTION_HEADER
#define MONITEUR_LISTEOPTION_HEADER

#include "Moniteur.h"

namespace Moniteur {
	struct ListeOption {
		char* message;
		void (*selection)(void* argument_generique);
	};
}

#endif
