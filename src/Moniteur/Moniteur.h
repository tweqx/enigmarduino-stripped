#ifndef MONITEUR_HEADER
#define MONITEUR_HEADER

#include <Arduino.h>

#include "../Utils/ROMString.h"

// Namespace visant à proposer un wrapper autour du
//  port série, en proposant des fonctions pour
//  faciliter l'interraction avec l'utilisateur
//  dans le code.
namespace Moniteur {
	// init()
	constexpr long BaudRate = 9600;
	void init();

	// print() & println()
	template<typename T>
	void print(const T& message) {
		Serial.print(message);
	}
	template<>
	void print<ROMString>(const ROMString&);
	template<>
	void print<char>(const char&);
	template<typename T>
	void println(T message) {
		print(message);
		print('\n');
		print('\r');
	}

	// printHex()
	// Affiche le nombre passé en entrée en hexadécimal, sans "0x"
	template<typename T>
	void printHex(T nbr) {
		for (int i = sizeof(T) - 1; i >= 0; i--) {
			unsigned char b = (nbr >> (i * 8)) & 0xff;

			if ((b & 0b11110000) >> 4 < 10)
				Moniteur::print<char>('0' + ((b & 0b11110000) >> 4));
			else
				Moniteur::print<char>('a' + ((b & 0b11110000) >> 4) - 10);

			if ((b & 0b00001111) < 10)
				Moniteur::print<char>('0' + (b & 0b00001111));
			else
				Moniteur::print<char>('a' + (b & 0b00001111) - 10);
		}
	}

	// lectureNombre() & lectureString() & attendreEntree()
	//  Note: les deux premières fonctions affichent dans le port série l'entrée reçue par l'utilisateur, afin de lui donner un retour sur ce qu'il a entré.
	//   La seconde ne retourne ni n'affiche pas l'entrée de l'utilisateur
	int lectureNombre();
	String lectureString();
	void attendreEntree();

	// lectureNombreHex()
	// Lit un nombre hexadécimal, de 'nbr_octets' octets, et le retourne
	// Cette fonction supporte éventuellement "0x" comme préfixe, et ne retourne pas tant que l'entrée n'est pas valide
	unsigned long long lectureNombreHex(unsigned int nbr_octets);

	// expect(): lit une entrée de l'utilisateur, et retourne true si elle correspond à l'entrée attendue
	//  dans tout les cas, comme pour les fonctions de lecture, l'entrée de l'utilisateur est affichée
	bool expect(const String& entree);
	bool expect(const ROMString& entree);

	// ListeOption & liste()
	struct ListeOption;
	void liste(const ListeOption liste[], const unsigned int& nbr_elements);
};

// On inclut ListeOption.h après avoir défini le namespace Moniteur, car ListeOptions.h
//  dépend du namespace Moniteur
#include "ListeOption.h"

#endif
