#ifndef PANNEAU_ADMIN_HEADER
#define PANNEAU_ADMIN_HEADER

#include <EEPROM.h>

#include "Moniteur/Moniteur.h"
#include "Enigmes/GestionEEPROM.h"

namespace PanneauAdmin {
	// Affiche le panneau administrateurs
	void menu();

	// Fonctionnalités avancée du panneau admin
	namespace Options {
		void editeurHexa();
	}
}

#endif
